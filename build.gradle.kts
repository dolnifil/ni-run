import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.4.31"
    kotlin("plugin.serialization") version "1.4.31"
}

application {
    mainClass.set("com.filipdolnik.cvut.run.MainKt")
}

group = "com.filipdolnik.cvut.run"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")

    testImplementation(kotlin("test-junit5"))
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.23.1")
}

tasks.withType(KotlinCompile::class).all {
    kotlinOptions {
        jvmTarget = "1.8"
        languageVersion = "1.5"
        apiVersion = "1.5"
        freeCompilerArgs = listOf("-Xopt-in=kotlin.ExperimentalUnsignedTypes", "-Xinline-classes")
    }
}

project.tasks.withType<Test> {
    useJUnitPlatform()
}

val run: JavaExec by tasks
run.standardInput = System.`in`

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "com.filipdolnik.cvut.run.MainKt"
    }

    from(
        project.configurations.named<Configuration>("runtimeClasspath").get()
            .map { if (it.isDirectory) it else project.zipTree(it) }
    )
}