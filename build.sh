#!/bin/bash

git submodule update --init --recursive

./gradlew assemble

cp build/libs/NI-RUN-1.0.jar program.jar

cd FML; ./build.sh
