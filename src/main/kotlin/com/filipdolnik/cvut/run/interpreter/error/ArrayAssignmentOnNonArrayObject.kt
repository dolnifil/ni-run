package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class ArrayAssignmentOnNonArrayObject(value: Value):
    InterpreterRuntimeError("Array assignment can be performed only on arrays. Was $value.")