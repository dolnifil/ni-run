package com.filipdolnik.cvut.run.interpreter.error

class WrongEscapeSequence(format: String): InterpreterRuntimeError("Format string \"$format\" contains unknown escape sequence.")