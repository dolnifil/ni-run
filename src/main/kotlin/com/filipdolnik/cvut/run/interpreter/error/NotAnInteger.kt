package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class NotAnInteger(value: Value): InterpreterRuntimeError("Value $value is not an integer.")