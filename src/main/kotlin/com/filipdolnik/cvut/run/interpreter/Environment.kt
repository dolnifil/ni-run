package com.filipdolnik.cvut.run.interpreter

import com.filipdolnik.cvut.run.ast.Identifier
import java.io.Writer

class Environment(private val stdout: Writer) {

    private val globalScope = Scope()

    var currentScope: Scope = globalScope
        private set

    fun valueForIdentifier(identifier: Identifier): Value = currentScope[identifier]

    fun setIdentifierValue(identifier: Identifier, value: Value) {
        currentScope[identifier] = value
    }

    fun declareIdentifier(identifier: Identifier, initialValue: Value) {
        currentScope.declareIdentifier(identifier, initialValue)
    }

    fun output(value: Value) {
        stdout.write("$value")
    }

    fun function(block: () -> Value): Value = withTemporaryScope(globalScope.nested(), block)

    fun method(receiver: Value.Object, method: Value.Method, block: () -> Value): Value =
        withTemporaryScope(Scope(globalScope, method.staticReceiver, receiver), block)

    fun block(block: () -> Value): Value = withTemporaryScope(currentScope.nested(), block)

    private fun withTemporaryScope(scope: Scope, block: () -> Value): Value {
        val previousScope = currentScope
        currentScope = scope

        val result = block()

        currentScope = previousScope

        return result
    }
}