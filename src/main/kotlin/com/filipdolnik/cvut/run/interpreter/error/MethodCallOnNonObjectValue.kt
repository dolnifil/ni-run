package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class MethodCallOnNonObjectValue(value: Value): InterpreterRuntimeError("Method call can be performed only on objects. Was $value.")