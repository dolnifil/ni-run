package com.filipdolnik.cvut.run.interpreter.error

abstract class InterpreterRuntimeError(message: String): RuntimeException(message)