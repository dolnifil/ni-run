package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class ArraySizeMustBePositiveInteger(value: Value): InterpreterRuntimeError("Array size must be a positive integer. Was $value.")