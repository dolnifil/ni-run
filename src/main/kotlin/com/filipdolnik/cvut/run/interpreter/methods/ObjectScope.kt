package com.filipdolnik.cvut.run.interpreter.methods

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.interpreter.Value
import com.filipdolnik.cvut.run.interpreter.error.FieldNotFound
import com.filipdolnik.cvut.run.interpreter.error.IdentifierAlreadyDeclared
import com.filipdolnik.cvut.run.interpreter.error.MethodNotFound

class ObjectScope(private val parentScope: ObjectScope? = null) {

    val fields: List<Field>
        get() = fieldByIdentifier.map { Field(it.key, it.value) }

    private val methodByIdentifier = mutableMapOf<Identifier, Value.Method>()
    private val fieldByIdentifier = mutableMapOf<Identifier, Value>()

    fun getMethod(identifier: Identifier): Value.Method =
        methodByIdentifier[identifier] ?: (parentScope?.getMethod(identifier) ?: throw MethodNotFound(identifier))

    fun getField(identifier: Identifier): Value =
        fieldByIdentifier[identifier] ?: throw FieldNotFound(identifier)

    fun setField(identifier: Identifier, value: Value) {
        if (identifier in fieldByIdentifier) {
            fieldByIdentifier[identifier] = value
        } else {
            throw FieldNotFound(identifier)
        }
    }

    fun declareField(identifier: Identifier, initialValue: Value) {
        if (identifier in methodByIdentifier || identifier in fieldByIdentifier) {
            throw IdentifierAlreadyDeclared(identifier)
        }

        fieldByIdentifier[identifier] = initialValue
    }

    fun declareMethod(method: Value.Method) {
        if (method.function.name in methodByIdentifier || method.function.name in fieldByIdentifier) {
            throw IdentifierAlreadyDeclared(method.function.name)
        }

        methodByIdentifier[method.function.name] = method
    }

    data class Field(val identifier: Identifier, val value: Value)
}