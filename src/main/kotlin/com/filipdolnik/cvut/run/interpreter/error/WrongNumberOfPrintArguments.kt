package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class WrongNumberOfPrintArguments(format: String, arguments: List<Value>):
    InterpreterRuntimeError("Print called with incorrect number of arguments. Format: \"$format\"." +
        "arguments: \"${arguments.joinToString(", ")}\"}")