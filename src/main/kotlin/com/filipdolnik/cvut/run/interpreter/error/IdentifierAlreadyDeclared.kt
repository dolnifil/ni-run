package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.ast.Identifier

class IdentifierAlreadyDeclared(identifierName: Identifier):
    InterpreterRuntimeError("Identifier $identifierName was already declared in current scope.")