package com.filipdolnik.cvut.run.interpreter

import com.filipdolnik.cvut.run.ast.AstNode
import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.interpreter.methods.ObjectScope
import com.filipdolnik.cvut.run.interpreter.methods.booleanMethodScope
import com.filipdolnik.cvut.run.interpreter.methods.integerMethodScope
import com.filipdolnik.cvut.run.interpreter.methods.objectMethodScope

sealed interface Value {

    fun asBoolean(): Object.Boolean

    sealed interface Object: Value {

        val objectScope: ObjectScope

        fun asIntegerOrNull(): Integer?

        data class Integer(val value: Int): Object {

            override val objectScope: ObjectScope = integerMethodScope(this)

            override fun asBoolean(): Boolean = Boolean(true)

            override fun asIntegerOrNull(): Integer = this

            override fun toString(): kotlin.String = value.toString()
        }

        data class Boolean(val value: kotlin.Boolean): Object {

            override val objectScope: ObjectScope = booleanMethodScope(this)

            override fun asBoolean(): Boolean = this

            override fun asIntegerOrNull(): Integer? = null

            override fun toString(): kotlin.String = value.toString()
        }

        data class Array(val data: MutableList<Value>): Object {

            override val objectScope: ObjectScope = objectMethodScope(this)

            override fun asBoolean(): Boolean = Boolean(true)

            override fun asIntegerOrNull(): Integer? = null

            override fun toString(): kotlin.String = "[${data.joinToString(", ")}]"
        }

        object Null: Object {

            override val objectScope: ObjectScope = objectMethodScope(this)

            override fun asBoolean(): Boolean = Boolean(false)

            override fun asIntegerOrNull(): Integer? = null

            override fun toString(): kotlin.String = "null"
        }

        class Custom(
            override val objectScope: ObjectScope,
            private val parent: Object,
        ): Object {

            override fun asBoolean(): Boolean = Boolean(true)

            override fun asIntegerOrNull(): Integer? = parent.asIntegerOrNull()

            override fun toString(): kotlin.String =
                "object(" +
                    "${
                        (objectScope.fields.map { it.identifier.name to it.value } + (".." to parent))
                            .joinToString(", ") {
                                "${it.first}=${it.second}"
                            }
                    })"
        }
    }

    data class String(val value: kotlin.String): Value {

        override fun asBoolean(): Object.Boolean = Object.Boolean(true)

        override fun toString(): kotlin.String = value
    }

    sealed interface Function: Value {

        val name: Identifier

        val parameterCount: Int

        class BuildIn(
            override val name: Identifier,
            override val parameterCount: Int,
            val action: (scope: Scope, arguments: List<Value>) -> Value,
        ): Function {

            override fun toString(): kotlin.String = "${name.name}()"
        }

        class Custom(val declaration: AstNode.Function): Function {

            override val name: Identifier = declaration.name

            override val parameterCount: Int = declaration.parameters.size

            override fun toString(): kotlin.String = "Function(${declaration.parameters.joinToString(",") { it.name }})"
        }

        override fun asBoolean(): Object.Boolean = Object.Boolean(true)
    }

    class Method(val staticReceiver: Object, val function: Function): Value {

        override fun asBoolean(): Object.Boolean = Object.Boolean(true)

        override fun toString(): kotlin.String = function.toString()
    }
}