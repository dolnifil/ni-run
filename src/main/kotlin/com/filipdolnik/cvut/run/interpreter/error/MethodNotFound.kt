package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.ast.Identifier

class MethodNotFound(name: Identifier): InterpreterRuntimeError("Method with identifier \"$name\" was not found.")