package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class CannotExtendNonObjectValue(value: Value): InterpreterRuntimeError("Objects can extend only other objects. Was $value.")