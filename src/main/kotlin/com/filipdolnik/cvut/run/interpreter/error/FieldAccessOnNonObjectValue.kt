package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class FieldAccessOnNonObjectValue(value: Value): InterpreterRuntimeError("Field access can be performed only on objects. Was $value.")