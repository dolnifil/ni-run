package com.filipdolnik.cvut.run.interpreter

class Memory {

    class Reference(initialValue: Value) {

        private var value = initialValue

        fun read(): Value = value

        fun write(value: Value) {
            this.value = value
        }
    }
}