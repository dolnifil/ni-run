package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class ArrayIndexOutOfBounds(value: Value, size: Int):
    InterpreterRuntimeError("Array index must be between between 0 and ${size - 1}. Was $value.")