package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.ast.Identifier

class FieldNotFound(name: Identifier): InterpreterRuntimeError("Field with identifier \"$name\" was not found.")