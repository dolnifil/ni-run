package com.filipdolnik.cvut.run.interpreter

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.interpreter.error.IdentifierAlreadyDeclared
import com.filipdolnik.cvut.run.interpreter.error.UnknownIdentifier

class Scope(
    private val parentScope: Scope? = null,
    val staticReceiver: Value.Object? = null,
    val dynamicReceiver: Value.Object? = null,
) {

    val staticReceiverOrError: Value.Object by lazy {
        staticReceiver ?: throw IllegalStateException("Cannot access receiver. Scope does not belong to a method.")
    }

    val dynamicReceiverOrError: Value.Object by lazy {
        dynamicReceiver ?: throw IllegalStateException("Cannot access receiver. Scope does not belong to a method.")
    }

    private val storage = mutableMapOf<Identifier, Memory.Reference>()

    init {
        if (dynamicReceiver != null) {
            declareIdentifier(Identifier("this"), dynamicReceiver)
        }
    }

    private fun getReference(identifier: Identifier): Memory.Reference? =
        storage[identifier] ?: parentScope?.getReference(identifier)

    operator fun get(identifier: Identifier): Value =
        getReference(identifier)?.read() ?: Value.Object.Null

    operator fun set(identifier: Identifier, value: Value) {
        val reference = getReference(identifier)
        if (reference != null) {
            reference.write(value)
        } else {
            throw UnknownIdentifier(identifier)
        }
    }

    fun declareIdentifier(identifier: Identifier, initialValue: Value) {
        if (identifier in storage) {
            throw IdentifierAlreadyDeclared(identifier)
        }

        storage[identifier] = Memory.Reference(initialValue)
    }

    fun nested(): Scope = Scope(this, staticReceiver, dynamicReceiver)
}