package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class NonFunctionCall(value: Value): InterpreterRuntimeError("Cannot call $value as it is not a function.")