package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.ast.Identifier

class UnknownIdentifier(name: Identifier): InterpreterRuntimeError("Identifier \"$name\" not found.")