package com.filipdolnik.cvut.run.interpreter.methods

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.interpreter.Value
import com.filipdolnik.cvut.run.interpreter.error.NotAnInteger

fun objectMethodScope(receiver: Value.Object): ObjectScope = ObjectScope().apply {
    listOf(
        Value.Function.BuildIn(Identifier("=="), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError == arguments.first())
        },
        Value.Function.BuildIn(Identifier("!="), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError != arguments.first())
        }
    ).forEach {
        declareMethod(Value.Method(receiver, it))
    }
}

fun booleanMethodScope(receiver: Value.Object): ObjectScope = ObjectScope(objectMethodScope(receiver)).apply {
    listOf(
        Value.Function.BuildIn(Identifier("&"), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError.asBoolean().value and arguments.first().asBoolean().value)
        },
        Value.Function.BuildIn(Identifier("|"), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError.asBoolean().value or arguments.first().asBoolean().value)
        }
    ).forEach {
        declareMethod(Value.Method(receiver, it))
    }
}

fun integerMethodScope(receiver: Value.Object): ObjectScope = ObjectScope(objectMethodScope(receiver)).apply {
    listOf(
        Value.Function.BuildIn(Identifier("+"), 1) { scope, arguments ->
            Value.Object.Integer(scope.dynamicReceiverOrError.asInteger().value + arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier("-"), 1) { scope, arguments ->
            Value.Object.Integer(scope.dynamicReceiverOrError.asInteger().value - arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier("*"), 1) { scope, arguments ->
            Value.Object.Integer(scope.dynamicReceiverOrError.asInteger().value * arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier("/"), 1) { scope, arguments ->
            Value.Object.Integer(scope.dynamicReceiverOrError.asInteger().value / arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier("%"), 1) { scope, arguments ->
            Value.Object.Integer(scope.dynamicReceiverOrError.asInteger().value % arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier("<="), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError.asInteger().value <= arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier(">="), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError.asInteger().value >= arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier("<"), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError.asInteger().value < arguments.first().asInteger().value)
        },
        Value.Function.BuildIn(Identifier(">"), 1) { scope, arguments ->
            Value.Object.Boolean(scope.dynamicReceiverOrError.asInteger().value > arguments.first().asInteger().value)
        },
    ).forEach {
        declareMethod(Value.Method(receiver, it))
    }
}

private fun Value.asInteger(): Value.Object.Integer =
    (this as? Value.Object)?.asIntegerOrNull() ?: throw NotAnInteger(this)