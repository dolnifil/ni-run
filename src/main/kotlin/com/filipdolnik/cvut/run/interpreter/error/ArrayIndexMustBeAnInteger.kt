package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class ArrayIndexMustBeAnInteger(value: Value): InterpreterRuntimeError("Array index must be an integer. Was $value.")