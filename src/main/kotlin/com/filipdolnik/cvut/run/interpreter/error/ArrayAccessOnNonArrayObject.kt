package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class ArrayAccessOnNonArrayObject(value: Value): InterpreterRuntimeError("Array access can be performed only on arrays. Was $value.")