package com.filipdolnik.cvut.run.interpreter

import com.filipdolnik.cvut.run.ast.AstNode
import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.interpreter.error.ArrayAccessOnNonArrayObject
import com.filipdolnik.cvut.run.interpreter.error.ArrayAssignmentOnNonArrayObject
import com.filipdolnik.cvut.run.interpreter.error.ArrayIndexMustBeAnInteger
import com.filipdolnik.cvut.run.interpreter.error.ArrayIndexOutOfBounds
import com.filipdolnik.cvut.run.interpreter.error.ArraySizeMustBePositiveInteger
import com.filipdolnik.cvut.run.interpreter.error.CannotExtendNonObjectValue
import com.filipdolnik.cvut.run.interpreter.error.FieldAccessOnNonObjectValue
import com.filipdolnik.cvut.run.interpreter.error.MethodCallOnNonObjectValue
import com.filipdolnik.cvut.run.interpreter.error.NonFunctionCall
import com.filipdolnik.cvut.run.interpreter.error.WrongEscapeSequence
import com.filipdolnik.cvut.run.interpreter.error.WrongNumberOfArguments
import com.filipdolnik.cvut.run.interpreter.error.WrongNumberOfPrintArguments
import com.filipdolnik.cvut.run.interpreter.methods.ObjectScope
import java.io.Writer

class Interpreter {

    fun run(program: AstNode, stdout: Writer) {
        val environment = Environment(stdout)

        program.evaluate(environment)
    }

    private fun AstNode.evaluate(environment: Environment): Value = when (this) {
        is AstNode.Integer -> evaluate()
        is AstNode.Boolean -> evaluate()
        AstNode.Null -> Value.Object.Null
        is AstNode.VariableDefinition -> evaluate(environment)
        is AstNode.VariableAccess -> evaluate(environment)
        is AstNode.VariableAssignment -> evaluate(environment)
        is AstNode.Array -> evaluate(environment)
        is AstNode.ArrayAccess -> evaluate(environment)
        is AstNode.ArrayAssignment -> evaluate(environment)
        is AstNode.Function -> evaluate(environment)
        is AstNode.FunctionCall -> evaluate(environment)
        is AstNode.Print -> evaluate(environment)
        is AstNode.Block -> evaluate(environment)
        is AstNode.Top -> evaluate(environment)
        is AstNode.While -> evaluate(environment)
        is AstNode.If -> evaluate(environment)
        is AstNode.Object -> evaluate(environment)
        is AstNode.FieldAssignment -> evaluate(environment)
        is AstNode.FieldAccess -> evaluate(environment)
        is AstNode.MethodCall -> evaluate(environment)
    }

    private fun AstNode.Integer.evaluate(): Value = Value.Object.Integer(value)

    private fun AstNode.Boolean.evaluate(): Value = Value.Object.Boolean(value)

    private fun AstNode.VariableDefinition.evaluate(environment: Environment): Value {
        val initialValue = value.evaluate(environment)

        environment.declareIdentifier(name, initialValue)

        return initialValue
    }

    private fun AstNode.VariableAccess.evaluate(environment: Environment): Value =
        environment.valueForIdentifier(name)

    private fun AstNode.VariableAssignment.evaluate(environment: Environment): Value {
        val newValue = value.evaluate(environment)

        environment.setIdentifierValue(name, newValue)

        return newValue
    }

    private fun AstNode.Array.evaluate(environment: Environment): Value {
        val size = size.evaluate(environment)
        if (size !is Value.Object.Integer || size.value <= 0) {
            throw ArraySizeMustBePositiveInteger(size)
        }

        val elements = (0 until size.value).map {
            environment.block {
                value.evaluate(environment)
            }
        }.toMutableList()

        return Value.Object.Array(elements)
    }

    private fun AstNode.ArrayAccess.evaluate(environment: Environment): Value {
        val array = array.evaluate(environment)
        if (array is Value.Object.Custom) {
            return methodCall(array, Identifier("get"), listOf(index), environment)
        }
        if (array !is Value.Object.Array) {
            throw ArrayAccessOnNonArrayObject(array)
        }

        val index = index.evaluate(environment)
        if (index !is Value.Object.Integer) {
            throw ArrayIndexMustBeAnInteger(index)
        }
        if (index.value !in array.data.indices) {
            throw ArrayIndexOutOfBounds(index, array.data.size)
        }

        return array.data[index.value]
    }

    private fun AstNode.ArrayAssignment.evaluate(environment: Environment): Value {
        val array = array.evaluate(environment)
        if (array is Value.Object.Custom) {
            return methodCall(array, Identifier("set"), listOf(index, value), environment)
        }
        if (array !is Value.Object.Array) {
            throw ArrayAssignmentOnNonArrayObject(array)
        }

        val index = index.evaluate(environment)
        if (index !is Value.Object.Integer) {
            throw ArrayIndexMustBeAnInteger(index)
        }
        if (index.value !in array.data.indices) {
            throw ArrayIndexOutOfBounds(index, array.data.size)
        }

        val value = value.evaluate(environment)

        array.data[index.value] = value

        return value
    }

    private fun AstNode.Function.evaluate(environment: Environment): Value {
        val functionDeclaration = Value.Function.Custom(this)

        environment.declareIdentifier(name, functionDeclaration)

        return Value.Object.Null
    }

    private fun AstNode.FunctionCall.evaluate(environment: Environment): Value {
        val target = environment.valueForIdentifier(name)
        if (target !is Value.Function) {
            throw NonFunctionCall(target)
        }

        val arguments = arguments.map { it.evaluate(environment) }
        if (arguments.size != target.parameterCount) {
            throw WrongNumberOfArguments(target, arguments)
        }

        return environment.function {
            when (target) {
                is Value.Function.BuildIn -> target.action(environment.currentScope, arguments)
                is Value.Function.Custom -> {
                    target.declaration.parameters.zip(arguments).forEach { (parameter, argument) ->
                        environment.declareIdentifier(parameter, argument)
                    }

                    target.declaration.body.evaluate(environment)
                }
            }
        }
    }

    private fun AstNode.Print.evaluate(environment: Environment): Value {
        val output = StringBuilder()
        var escaped = false

        val evaluatedArguments = arguments.map { it.evaluate(environment) }
        val reversedArguments = evaluatedArguments.reversed().toMutableList()

        format.forEach {
            if (escaped) {
                val character = when (it) {
                    '~' -> '~'
                    'n' -> '\n'
                    '"' -> '"'
                    'r' -> '\r'
                    't' -> '\t'
                    '\\' -> '\\'
                    else -> throw WrongEscapeSequence(format)
                }
                output.append(character)

                escaped = false
            } else {
                when (it) {
                    '\\' -> escaped = true
                    '~' -> {
                        val argument = reversedArguments.removeLastOrNull() ?: throw WrongNumberOfPrintArguments(format, evaluatedArguments)

                        output.append(argument)
                    }
                    else -> {
                        output.append(it)
                    }
                }
            }
        }

        if (escaped) {
            throw WrongEscapeSequence(format)
        }
        if (reversedArguments.isNotEmpty()) {
            throw WrongNumberOfPrintArguments(format, evaluatedArguments)
        }

        environment.output(Value.String(output.toString()))

        return Value.Object.Null
    }

    private fun AstNode.Block.evaluate(environment: Environment): Value = environment.block {
        var result: Value = Value.Object.Null

        body.forEach {
            result = it.evaluate(environment)
        }

        result
    }

    private fun AstNode.Top.evaluate(environment: Environment): Value {
        body.forEach {
            it.evaluate(environment)
        }

        return Value.Object.Null
    }

    private fun AstNode.While.evaluate(environment: Environment): Value {
        while (condition.evaluate(environment).asBoolean().value) {
            body.evaluate(environment)
        }

        return Value.Object.Null
    }

    private fun AstNode.If.evaluate(environment: Environment): Value = if (condition.evaluate(environment).asBoolean().value) {
        thenBranch.evaluate(environment)
    } else {
        elseBranch.evaluate(environment)
    }

    private fun AstNode.Object.evaluate(environment: Environment): Value {
        val parent = parent.evaluate(environment)
        if (parent !is Value.Object) {
            throw CannotExtendNonObjectValue(parent)
        }

        val objectScope = ObjectScope(parent.objectScope)
        val customObject = Value.Object.Custom(objectScope, parent)

        body.forEach {
            when (it) {
                is AstNode.VariableDefinition -> {
                    val initialValue = it.value.evaluate(environment)

                    objectScope.declareField(it.name, initialValue)
                }
                is AstNode.Function -> {
                    val function = Value.Function.Custom(it)

                    objectScope.declareMethod(Value.Method(customObject, function))
                }
                else -> throw IllegalStateException("Unexpected object body node $it.")
            }
        }

        return customObject
    }

    private fun AstNode.FieldAssignment.evaluate(environment: Environment): Value {
        val receiver = if (receiver is AstNode.VariableAccess && receiver.name == Identifier("this")) {
            environment.currentScope.staticReceiver
        } else {
            null
        } ?: receiver.evaluate(environment)

        if (receiver !is Value.Object) {
            throw FieldAccessOnNonObjectValue(receiver)
        }

        val value = value.evaluate(environment)

        receiver.objectScope.setField(field, value)

        return value
    }

    private fun AstNode.FieldAccess.evaluate(environment: Environment): Value {
        val receiver = if (receiver is AstNode.VariableAccess && receiver.name == Identifier("this")) {
            environment.currentScope.staticReceiver
        } else {
            null
        } ?: receiver.evaluate(environment)

        if (receiver !is Value.Object) {
            throw FieldAccessOnNonObjectValue(receiver)
        }

        return receiver.objectScope.getField(field)
    }

    private fun AstNode.MethodCall.evaluate(environment: Environment): Value {
        val receiver = receiver.evaluate(environment)
        if (receiver !is Value.Object) {
            throw MethodCallOnNonObjectValue(receiver)
        }

        return methodCall(receiver, name, arguments, environment)
    }

    private fun methodCall(receiver: Value.Object, methodName: Identifier, argumentNodes: List<AstNode>, environment: Environment): Value {
        val method = receiver.objectScope.getMethod(methodName)

        val arguments = argumentNodes.map { it.evaluate(environment) }
        if (arguments.size != method.function.parameterCount) {
            throw WrongNumberOfArguments(method.function, arguments)
        }

        return environment.method(receiver, method) {
            when (method.function) {
                is Value.Function.BuildIn -> method.function.action(environment.currentScope, arguments)
                is Value.Function.Custom -> {
                    method.function.declaration.parameters.zip(arguments).forEach { (parameter, argument) ->
                        environment.declareIdentifier(parameter, argument)
                    }

                    method.function.declaration.body.evaluate(environment)
                }
            }
        }
    }
}

