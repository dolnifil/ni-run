package com.filipdolnik.cvut.run.interpreter.error

import com.filipdolnik.cvut.run.interpreter.Value

class WrongNumberOfArguments(function: Value.Function, arguments: List<Value>):
    InterpreterRuntimeError("Function ${function.name} called with incorrect number of arguments. " +
        "Expected: ${function.parameterCount}. Was: \"${arguments.joinToString(", ")}\"}")