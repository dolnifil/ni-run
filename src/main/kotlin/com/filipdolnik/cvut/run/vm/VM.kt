package com.filipdolnik.cvut.run.vm

import com.filipdolnik.cvut.run.vm.frame.FrameStack
import com.filipdolnik.cvut.run.vm.memory.GlobalVariables
import com.filipdolnik.cvut.run.vm.memory.OperandStack
import com.filipdolnik.cvut.run.vm.memory.heap.Heap
import com.filipdolnik.cvut.run.vm.memory.heap.HeapWithGC
import com.filipdolnik.cvut.run.vm.memory.heap.InfiniteHeap
import com.filipdolnik.cvut.run.vm.parser.BytecodeParser
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.io.Writer

class VM(
    private val parsedBytecode: BytecodeParser.ParsedBytecode,
) {

    fun run(stdout: Writer, heapSize: Int?, heapLog: String?) {
        heapLog?.let { OutputStreamWriter(FileOutputStream(it)) }.use { heapLogStream ->
            val heapFactory: (FrameStack, GlobalVariables, OperandStack) -> Heap = if (heapSize != null) {
                { frameStack, globalVariables, operandStack ->
                    HeapWithGC(heapSize, heapLogStream, frameStack, globalVariables, operandStack)
                }
            } else {
                { _, _, _ -> InfiniteHeap() }
            }

            val environment = Environment(parsedBytecode, heapFactory, stdout)

            while (environment.hasNextInstruction) {
                val instruction = environment.nextInstruction()

                environment.execute(instruction)
            }

            heapLogStream?.flush()
        }
    }
}
