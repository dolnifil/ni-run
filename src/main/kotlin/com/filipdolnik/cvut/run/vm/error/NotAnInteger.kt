package com.filipdolnik.cvut.run.vm.error

import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

class NotAnInteger(runtimeObject: RuntimeObject): VMRuntimeError("Object $runtimeObject is not an integer.")