package com.filipdolnik.cvut.run.vm.memory.heap

import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.frame.FrameStack
import com.filipdolnik.cvut.run.vm.memory.GlobalVariables
import com.filipdolnik.cvut.run.vm.memory.OperandStack
import com.filipdolnik.cvut.run.vm.memory.RuntimeObject
import java.io.OutputStreamWriter

class HeapWithGC(
    heapSizeInMib: Int,
    private val heapLog: OutputStreamWriter?,
    private val frameStack: FrameStack,
    private val globalVariables: GlobalVariables,
    private val operandStack: OperandStack,
): Heap {

    override val byteSize: Int
        get() = from.byteSize

    override val objectCount: Int
        get() = from.objectCount

    private val heapSize = heapSizeInMib * 1024 * 1024
    private val logAfterAllocatedBytes = 10 * 1024

    private var from = InfiniteHeap()

    private var to = InfiniteHeap()

    private val forwardingPointers = mutableListOf<Heap.Pointer?>()
    private val openPointers = mutableListOf<Heap.Pointer>()

    private var lastLoggedSize = 0;

    init {
        heapLog?.let {
            it.write("timestamp,event,heap\n")
            it.write("${System.nanoTime()},S,$byteSize\n")
        }
    }

    override fun get(pointer: Heap.Pointer): RuntimeObject = from[pointer]

    override fun allocate(literal: ProgramObject): Heap.Pointer = mutateHeap {
        from.allocate(literal)
    }

    override fun allocate(runtimeObject: RuntimeObject): Heap.Pointer = mutateHeap {
        from.allocate(runtimeObject)
    }

    private fun mutateHeap(action: () -> Heap.Pointer): Heap.Pointer {
        val result = action()

        if (lastLoggedSize + logAfterAllocatedBytes < byteSize) {
            heapLog?.write("${System.nanoTime()},A,$byteSize\n")
            lastLoggedSize = byteSize
        }

        return if (from.byteSize > heapSize) {
            collect(result)
        } else {
            result
        }
    }

    private fun collect(allocatedPointer: Heap.Pointer): Heap.Pointer {
        if (lastLoggedSize < byteSize) {
            heapLog?.write("${System.nanoTime()},A,$byteSize\n")
        }

        repeat(from.objectCount) {
            forwardingPointers.add(null)
        }

        reallocateGlobalVariables()
        reallocateFrameStack()
        reallocateOperandStack()

        val result = relocate(allocatedPointer)
        while (openPointers.isNotEmpty()) {
            translateDependencies(openPointers.removeLast())
        }

        from = to
        to = InfiniteHeap()
        forwardingPointers.clear()

        heapLog?.write("${System.nanoTime()},G,$byteSize\n")
        lastLoggedSize = byteSize

        return result
    }

    private fun reallocateGlobalVariables() {
        globalVariables.variables.keys.toList().forEach {
            globalVariables.variables[it] = relocate(globalVariables.variables.getValue(it))
        }
    }

    private fun reallocateFrameStack() {
        frameStack.frames.forEach {
            for (index in it.localVariables.indices) {
                it.localVariables[index] = relocate(it.localVariables[index])
            }
        }
    }

    private fun reallocateOperandStack() {
        for (index in operandStack.data.indices) {
            operandStack.data[index] = relocate(operandStack.data[index])
        }
    }

    private fun relocate(pointer: Heap.Pointer): Heap.Pointer {
        forwardingPointers[pointer.value]?.let { return it }

        val newPointer = to.allocate(from[pointer])

        forwardingPointers[pointer.value] = newPointer
        openPointers.add(newPointer)

        return newPointer
    }

    private fun translateDependencies(pointer: Heap.Pointer) {
        when (val runtimeObject = to[pointer]) {
            is RuntimeObject.Array -> {
                for (index in runtimeObject.data.indices) {
                    runtimeObject.data[index] = relocate(runtimeObject.data[index])
                }
            }
            is RuntimeObject.Boolean -> {
            }
            is RuntimeObject.Integer -> {
            }
            RuntimeObject.Null -> {
            }
            is RuntimeObject.Object -> {
                runtimeObject.parent = relocate(runtimeObject.parent)
                for (index in runtimeObject.fields.indices) {
                    runtimeObject.fields[index] = relocate(runtimeObject.fields[index])
                }
            }
        }
    }
}