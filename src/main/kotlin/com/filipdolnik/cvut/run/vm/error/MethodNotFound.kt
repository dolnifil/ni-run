package com.filipdolnik.cvut.run.vm.error

import com.filipdolnik.cvut.run.vm.constant.ProgramObject

class MethodNotFound(name: ProgramObject.String): VMRuntimeError("Method with identifier \"${name.value}\" was not found.")