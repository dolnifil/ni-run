package com.filipdolnik.cvut.run.vm.memory

import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.memory.heap.Heap

sealed interface RuntimeObject {

    val size: Int

    fun convertToBoolean(heap: Heap): kotlin.Boolean

    fun asIntegerOrNull(heap: Heap): Integer?

    fun asBooleanOrNull(heap: Heap): Boolean?

    fun toString(heap: Heap, constantPool: ConstantPool): String

    object Null: RuntimeObject {

        override val size: Int
            get() = 1

        override fun convertToBoolean(heap: Heap): kotlin.Boolean = false

        override fun asIntegerOrNull(heap: Heap): Integer? = null

        override fun asBooleanOrNull(heap: Heap): Boolean? = null

        override fun toString(heap: Heap, constantPool: ConstantPool): String = "null"
    }

    inline class Integer(val value: Int): RuntimeObject {

        override val size: Int
            get() = 4

        override fun convertToBoolean(heap: Heap): kotlin.Boolean = true

        override fun asIntegerOrNull(heap: Heap): Integer = this

        override fun asBooleanOrNull(heap: Heap): Boolean? = null

        override fun toString(heap: Heap, constantPool: ConstantPool): String = value.toString()
    }

    inline class Boolean(val value: kotlin.Boolean): RuntimeObject {

        override val size: Int
            get() = 1

        override fun convertToBoolean(heap: Heap): kotlin.Boolean = value

        override fun asIntegerOrNull(heap: Heap): Integer? = null

        override fun asBooleanOrNull(heap: Heap): Boolean = this

        override fun toString(heap: Heap, constantPool: ConstantPool): String = value.toString()
    }

    inline class Array(val data: kotlin.Array<Heap.Pointer>): RuntimeObject {

        override val size: Int
            get() = 4 + data.size * 4

        override fun convertToBoolean(heap: Heap): kotlin.Boolean = true

        override fun asIntegerOrNull(heap: Heap): Integer? = null

        override fun asBooleanOrNull(heap: Heap): Boolean? = null

        override fun toString(heap: Heap, constantPool: ConstantPool): String = "[${
            data.joinToString(", ") {
                heap[it].toString(heap, constantPool)
            }
        }]"
    }

    class Object(var parent: Heap.Pointer, val type: Class, val fields: kotlin.Array<Heap.Pointer>): RuntimeObject {

        override val size: Int
            get() = 4 + 4 + fields.size * 4

        override fun convertToBoolean(heap: Heap): kotlin.Boolean = heap[parent].convertToBoolean(heap)

        override fun asIntegerOrNull(heap: Heap): Integer? = heap[parent].asIntegerOrNull(heap)

        override fun asBooleanOrNull(heap: Heap): Boolean? = heap[parent].asBooleanOrNull(heap)

        override fun toString(heap: Heap, constantPool: ConstantPool): String {
            val fieldNames = type.fieldsNameIndex.map { constantPool[it] as ProgramObject.String }

            return "object(" +
                "${
                    (fieldNames.zip(fields).map { it.first.value to heap[it.second].toString(heap, constantPool) } +
                        (".." to heap[parent].toString(heap, constantPool)))
                        .joinToString(", ") {
                            "${it.first}=${it.second}"
                        }
                })"
        }

        class Class(
            val fieldsNameIndex: List<ConstantPool.Index>,
            val methods: List<ConstantPool.Index>,
        )
    }
}