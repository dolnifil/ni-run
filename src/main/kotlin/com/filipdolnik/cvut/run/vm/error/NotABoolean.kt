package com.filipdolnik.cvut.run.vm.error

import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

class NotABoolean(runtimeObject: RuntimeObject): VMRuntimeError("Object $runtimeObject is not a boolean.")