package com.filipdolnik.cvut.run.vm.constant

class ConstantPool(private val data: List<ProgramObject>) {

    operator fun get(index: Index): ProgramObject = data[index.value.toInt()]

    inline class Index(val value: UShort)
}
