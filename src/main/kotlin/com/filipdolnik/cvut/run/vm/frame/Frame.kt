package com.filipdolnik.cvut.run.vm.frame

import com.filipdolnik.cvut.run.vm.code.InstructionPointer
import com.filipdolnik.cvut.run.vm.memory.heap.Heap
import com.filipdolnik.cvut.run.vm.memory.LocalVariables

class Frame(val returnAddress: InstructionPointer, localVariableCount: Int) {

    val localVariables: Array<Heap.Pointer> = Array(localVariableCount) { Heap.nullPointer }

    operator fun get(index: LocalVariables.Index): Heap.Pointer = localVariables[index.value.toInt()]

    operator fun set(index: LocalVariables.Index, pointer: Heap.Pointer) {
        if (index.value.toInt() !in localVariables.indices) {
            throw IndexOutOfBoundsException(
                "Cannot set local variable which was not allocated at the start. Size: ${localVariables.size}, index: ${pointer.value}"
            )
        }

        localVariables[index.value.toInt()] = pointer
    }
}