package com.filipdolnik.cvut.run.vm.code

inline class InstructionPointer(val value: Int) {

    fun bumped(by: Int): InstructionPointer = InstructionPointer(value + by)
}