package com.filipdolnik.cvut.run.vm.code

import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject

class Functions(
    private val functions: Map<ProgramObject.String, ProgramObject.Method>,
    private val constantPool: ConstantPool,
) {

    operator fun get(nameIndex: ConstantPool.Index): ProgramObject.Method =
        functions[name(nameIndex)] ?: throw IllegalArgumentException("Function with name ${name(nameIndex)} not found.")

    private fun name(nameIndex: ConstantPool.Index): ProgramObject.String =
        (constantPool[nameIndex] as? ProgramObject.String)
            ?: throw IllegalArgumentException("Function name must be a string. Was ${constantPool[nameIndex]}")
}