package com.filipdolnik.cvut.run.vm.error

import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

class ArrayIndexOutOfBounds(array: RuntimeObject.Array, index: RuntimeObject.Integer):
    VMRuntimeError("Array index must be between between 0 and ${array.data.size - 1}. Was $array[${index.value}].")