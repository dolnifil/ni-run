package com.filipdolnik.cvut.run.vm.parser

import com.filipdolnik.cvut.run.vm.code.Code
import com.filipdolnik.cvut.run.vm.code.InstructionPointer
import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.instruction.Instruction

class BytecodeParser {

    fun parse(bytecode: UByteArray): ParsedBytecode {
        val stream = UByteInputStream(bytecode)

        val (constantPool, code) = parseConstantPool(stream)
        val (globalVariables, functions) = parseGlobals(stream, constantPool)
        val mainFunction = entryPoint(stream, constantPool)

        return ParsedBytecode(
            constantPool,
            code,
            globalVariables,
            functions,
            mainFunction,
        )
    }

    private fun parseConstantPool(stream: UByteInputStream): Pair<ConstantPool, Code> {
        val programObjects = mutableListOf<ProgramObject>()
        val code = mutableListOf<UByte>()
        val labels = mutableMapOf<ConstantPool.Index, InstructionPointer>()

        val entryCount = stream.nextUShort().toInt()

        repeat(entryCount) {
            val programObject = parseProgramObject(stream, code, labels)

            programObjects.add(programObject)
        }

        val constantPool = ConstantPool(programObjects)
        val labelsWithName = labels.mapKeys {
            constantPool[it.key] as? ProgramObject.String
                ?: throw IllegalArgumentException("Label must point to a string. Was ${constantPool[it.key]}")
        }

        return constantPool to Code(code.toUByteArray(), labelsWithName)
    }

    private fun parseProgramObject(
        stream: UByteInputStream,
        code: MutableList<UByte>,
        labels: MutableMap<ConstantPool.Index, InstructionPointer>,
    ): ProgramObject =
        when (val tag = stream.nextUByte().toInt()) {
            0x00 -> ProgramObject.Integer(stream.nextInt())
            0x01 -> ProgramObject.Null
            0x02 -> {
                val length = stream.nextInt()
                val bytes = stream.next(length)
                val string = String(bytes.toUByteArray().toByteArray())
                ProgramObject.String(string)
            }
            0x03 -> parseMethod(stream, code, labels)
            0x04 -> ProgramObject.Slot(ConstantPool.Index(stream.nextUShort()))
            0x05 -> parseClass(stream)
            0x06 -> ProgramObject.Boolean(stream.nextUByte().toInt() == 0x01)
            else -> throw IllegalArgumentException("Unsupported program object tag $tag.")
        }

    private fun parseMethod(
        stream: UByteInputStream,
        code: MutableList<UByte>,
        labels: MutableMap<ConstantPool.Index, InstructionPointer>,
    ): ProgramObject {
        val nameIndex = ConstantPool.Index(stream.nextUShort())
        val argumentCount = ProgramObject.Method.ArgumentCount(stream.nextUByte())
        val localVariableCount = ProgramObject.Method.LocalVariableCount(stream.nextUShort())
        val entryPoint = InstructionPointer(code.size)

        val instructionCount = stream.nextInt()
        repeat(instructionCount) {
            val instructionCode = stream.nextUByte()

            if (instructionCode.toInt() == 0x00) {
                labels[ConstantPool.Index(stream.nextUShort())] = InstructionPointer(code.size)
            } else {
                val instructionLength = Instruction.from(instructionCode).length

                code.add(instructionCode)
                code.addAll(stream.next(instructionLength - 1))
            }
        }

        return ProgramObject.Method(nameIndex, argumentCount, localVariableCount, entryPoint)
    }

    private fun parseClass(stream: UByteInputStream): ProgramObject {
        val members = mutableListOf<ConstantPool.Index>()

        val memberCount = stream.nextUShort()
        repeat(memberCount.toInt()) {
            members.add(ConstantPool.Index(stream.nextUShort()))
        }

        return ProgramObject.Class(members)
    }

    private fun parseGlobals(
        stream: UByteInputStream,
        constantPool: ConstantPool,
    ): Pair<Set<ProgramObject.String>, Map<ProgramObject.String, ProgramObject.Method>> {
        val globalVariables = mutableSetOf<ProgramObject.String>()
        val functions = mutableMapOf<ProgramObject.String, ProgramObject.Method>()

        val length = stream.nextUShort().toInt()

        repeat(length) {
            val index = ConstantPool.Index(stream.nextUShort())

            when (val programObject = constantPool[index]) {
                is ProgramObject.Slot -> {
                    val name = (constantPool[programObject.nameIndex] as? ProgramObject.String)
                        ?: throw IllegalStateException("Name index ${programObject.nameIndex} of slot $index points to non string object.")

                    globalVariables.add(name)
                }
                is ProgramObject.Method -> {
                    val name = (constantPool[programObject.nameIndex] as? ProgramObject.String)
                        ?: throw IllegalStateException(
                            "Name index ${programObject.nameIndex} of method $index points to non string object."
                        )

                    functions[name] = programObject
                }
                else -> throw IllegalArgumentException("Global index $index points to unsupported object.")
            }
        }

        return globalVariables to functions
    }

    private fun entryPoint(stream: UByteInputStream, constantPool: ConstantPool): ProgramObject.Method {
        val index = ConstantPool.Index(stream.nextUShort())

        val method = constantPool[index]
        if (method !is ProgramObject.Method) {
            throw IllegalStateException("Entry point does not point to a method. Was: $method")
        }

        return method
    }

    data class ParsedBytecode(
        val constantPool: ConstantPool,
        val code: Code,
        val globalVariables: Set<ProgramObject.String>,
        val functions: Map<ProgramObject.String, ProgramObject.Method>,
        val mainFunction: ProgramObject.Method,
    )

    private class UByteInputStream(private val buffer: UByteArray) {

        private var index = 0

        fun nextUByte(): UByte = buffer[index++]

        fun nextUShort(): UShort {
            val lByte = nextUByte()
            val uByte = nextUByte()

            return (uByte.toInt().shl(8) or lByte.toInt()).toUShort()
        }

        fun nextInt(): Int {
            val byte0 = nextUByte().toInt()
            val byte1 = nextUByte().toInt()
            val byte2 = nextUByte().toInt()
            val byte3 = nextUByte().toInt()

            return byte3.shl(24) or byte2.shl(16) or byte1.shl(8) or byte0
        }

        fun next(n: Int): List<UByte> {
            val result = mutableListOf<UByte>()

            repeat(n) {
                result.add(nextUByte())
            }

            return result
        }
    }
}