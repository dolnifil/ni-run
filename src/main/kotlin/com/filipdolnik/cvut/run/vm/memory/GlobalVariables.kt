package com.filipdolnik.cvut.run.vm.memory

import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.memory.heap.Heap

class GlobalVariables(
    variables: Set<ProgramObject.String>,
    private val constantPool: ConstantPool,
) {

    val variables: MutableMap<ProgramObject.String, Heap.Pointer> = variables.associateWith { Heap.nullPointer }.toMutableMap()

    operator fun get(nameIndex: ConstantPool.Index): Heap.Pointer =
        variables[name(nameIndex)] ?: throw IllegalArgumentException("Global variable with name ${name(nameIndex)} not found.")

    operator fun set(nameIndex: ConstantPool.Index, pointer: Heap.Pointer) {
        val name = name(nameIndex)

        if (name !in variables) {
            throw IllegalArgumentException("Global variable with name ${name(nameIndex)} not found.")
        }

        variables[name] = pointer
    }

    private fun name(nameIndex: ConstantPool.Index): ProgramObject.String =
        (constantPool[nameIndex] as? ProgramObject.String)
            ?: throw IllegalArgumentException("Global variable name must be a string. Was ${constantPool[nameIndex]}")
}
