package com.filipdolnik.cvut.run.vm.instruction

import com.filipdolnik.cvut.run.vm.Environment
import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.error.ArrayIndexOutOfBounds
import com.filipdolnik.cvut.run.vm.error.MethodNotFound
import com.filipdolnik.cvut.run.vm.error.NotABoolean
import com.filipdolnik.cvut.run.vm.error.NotAnInteger
import com.filipdolnik.cvut.run.vm.memory.heap.Heap
import com.filipdolnik.cvut.run.vm.memory.RuntimeObject
import kotlin.reflect.KClass

sealed class BuiltInMethods<R: RuntimeObject>(val receiverClass: KClass<R>) {

    fun executeIn(
        environment: Environment,
        receiver: R,
        methodName: ProgramObject.String,
        arguments: List<Heap.Pointer>,
    ): Unit = environment.run {
        execute(receiver, methodName, arguments)
    }

    protected abstract fun Environment.execute(receiver: R, methodName: ProgramObject.String, arguments: List<Heap.Pointer>)

    object Null: BuiltInMethods<RuntimeObject.Null>(RuntimeObject.Null::class) {

        override fun Environment.execute(receiver: RuntimeObject.Null, methodName: ProgramObject.String, arguments: List<Heap.Pointer>) {
            val rhs = heap[arguments.first()]

            val result = when (methodName.value) {
                "==", "eq" -> rhs is RuntimeObject.Null
                "!=", "neq" -> rhs !is RuntimeObject.Null
                else -> throw MethodNotFound(methodName)
            }

            val pointer = heap.allocate(ProgramObject.Boolean(result))

            operandStack.push(pointer)
        }
    }

    object Integer: BuiltInMethods<RuntimeObject.Integer>(RuntimeObject.Integer::class) {

        override fun Environment.execute(receiver: RuntimeObject.Integer, methodName: ProgramObject.String, arguments: List<Heap.Pointer>) {
            val rhs = heap[arguments.first()]

            fun withRhsInteger(action: (Int) -> ProgramObject): ProgramObject =
                action(rhs.asIntegerOrNull(heap)?.value ?: throw NotAnInteger(rhs))

            val result = when (methodName.value) {
                "==", "eq" -> ProgramObject.Boolean(rhs is RuntimeObject.Integer && receiver.value == rhs.value)
                "!=", "neq" -> ProgramObject.Boolean(!(rhs is RuntimeObject.Integer && receiver.value == rhs.value))
                "+", "add" -> withRhsInteger { ProgramObject.Integer(receiver.value + it) }
                "-", "sub" -> withRhsInteger { ProgramObject.Integer(receiver.value - it) }
                "*", "mul" -> withRhsInteger { ProgramObject.Integer(receiver.value * it) }
                "/", "div" -> withRhsInteger { ProgramObject.Integer(receiver.value / it) }
                "%", "mod" -> withRhsInteger { ProgramObject.Integer(receiver.value % it) }
                "<=", "le" -> withRhsInteger { ProgramObject.Boolean(receiver.value <= it) }
                ">=", "ge" -> withRhsInteger { ProgramObject.Boolean(receiver.value >= it) }
                "<", "lt" -> withRhsInteger { ProgramObject.Boolean(receiver.value < it) }
                ">", "gt" -> withRhsInteger { ProgramObject.Boolean(receiver.value > it) }
                else -> throw MethodNotFound(methodName)
            }

            val pointer = heap.allocate(result)

            operandStack.push(pointer)
        }
    }

    object Boolean: BuiltInMethods<RuntimeObject.Boolean>(RuntimeObject.Boolean::class) {

        override fun Environment.execute(receiver: RuntimeObject.Boolean, methodName: ProgramObject.String, arguments: List<Heap.Pointer>) {
            val rhs = heap[arguments.first()]

            fun withRhsBoolean(action: (kotlin.Boolean) -> kotlin.Boolean): kotlin.Boolean =
                action(rhs.asBooleanOrNull(heap)?.value ?: throw NotABoolean(rhs))

            val result = when (methodName.value) {
                "==", "eq" -> rhs is RuntimeObject.Boolean && receiver.value == rhs.value
                "!=", "neq" -> !(rhs is RuntimeObject.Boolean && receiver.value == rhs.value)
                "&", "and" -> withRhsBoolean { receiver.value.and(it) }
                "|", "or" -> withRhsBoolean { receiver.value.or(it) }
                else -> throw MethodNotFound(methodName)
            }

            val pointer = heap.allocate(ProgramObject.Boolean(result))

            operandStack.push(pointer)
        }
    }

    object Array: BuiltInMethods<RuntimeObject.Array>(RuntimeObject.Array::class) {

        override fun Environment.execute(receiver: RuntimeObject.Array, methodName: ProgramObject.String, arguments: List<Heap.Pointer>) {
            val index = heap[arguments.first()].asIntegerOrNull(heap)
                ?: throw IllegalStateException("Index of an array must be an Integer. Was ${heap[arguments.first()]}")

            when (methodName.value) {
                "get" -> {
                    val pointer = receiver.data.getOrNull(index.value) ?: throw ArrayIndexOutOfBounds(receiver, index)

                    operandStack.push(pointer)
                }
                "set" -> {
                    val pointer = arguments[1]

                    if (index.value !in receiver.data.indices) {
                        throw ArrayIndexOutOfBounds(receiver, index)
                    }

                    receiver.data[index.value] = pointer

                    operandStack.push(pointer)
                }
                else -> throw MethodNotFound(methodName)
            }
        }
    }

    companion object {

        private fun all(): List<BuiltInMethods<*>> =
            BuiltInMethods::class.sealedSubclasses.map {
                it.objectInstance ?: throw IllegalStateException("All BuiltInMethod must be objects: $it")
            }

        private val map = all().associateBy {
            it.receiverClass
        }

        @Suppress("UNCHECKED_CAST")
        fun <T: RuntimeObject> resolve(receiver: T): BuiltInMethods<T>? = map[receiver::class] as BuiltInMethods<T>?
    }
}