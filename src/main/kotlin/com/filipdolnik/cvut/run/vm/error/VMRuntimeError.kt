package com.filipdolnik.cvut.run.vm.error

abstract class VMRuntimeError(message: String): RuntimeException(message)