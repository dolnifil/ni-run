package com.filipdolnik.cvut.run.vm.constant

import com.filipdolnik.cvut.run.vm.code.InstructionPointer

sealed interface ProgramObject {

    inline class Integer(val value: Int): ProgramObject

    inline class Boolean(val value: kotlin.Boolean): ProgramObject

    object Null: ProgramObject

    inline class String(val value: kotlin.String): ProgramObject

    inline class Slot(val nameIndex: ConstantPool.Index): ProgramObject

    data class Method(
        val nameIndex: ConstantPool.Index,
        val argumentCount: ArgumentCount,
        val localVariableCount: LocalVariableCount,
        val entryPoint: InstructionPointer,
    ): ProgramObject {

        val totalVariableCount: Int = (argumentCount.value + localVariableCount.value).toInt()

        inline class ArgumentCount constructor(val value: UByte)

        inline class LocalVariableCount constructor(val value: UShort)
    }

    inline class Class(val members: List<ConstantPool.Index>): ProgramObject
}