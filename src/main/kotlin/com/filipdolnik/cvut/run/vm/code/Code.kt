package com.filipdolnik.cvut.run.vm.code

import com.filipdolnik.cvut.run.vm.constant.ProgramObject

class Code(private val data: UByteArray, val labels: Map<ProgramObject.String, InstructionPointer>) {

    val indices: IntRange
        get() = data.indices

    operator fun get(instructionPointer: InstructionPointer, offset: Int = 0): UByte =
        data[instructionPointer.value + offset]
}