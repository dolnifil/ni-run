package com.filipdolnik.cvut.run.vm.memory.heap

import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

interface Heap {

    val byteSize: Int

    val objectCount: Int

    operator fun get(pointer: Pointer): RuntimeObject

    fun allocate(literal: ProgramObject): Pointer

    fun allocate(runtimeObject: RuntimeObject): Pointer

    companion object {

        val nullPointer: Pointer = Pointer(0)
    }

    inline class Pointer(val value: Int)
}