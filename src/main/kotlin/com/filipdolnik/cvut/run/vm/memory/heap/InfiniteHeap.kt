package com.filipdolnik.cvut.run.vm.memory.heap

import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

class InfiniteHeap: Heap {

    override var byteSize: Int = 0
        private set

    override val objectCount: Int
        get() = data.size

    private val data = mutableListOf<RuntimeObject>()

    init {
        data.add(RuntimeObject.Null)
        data.add(RuntimeObject.Boolean(false))
        data.add(RuntimeObject.Boolean(true))
        for (i in -128 until 128) {
            data.add(RuntimeObject.Integer(i))
        }

        byteSize = data.sumBy { it.size }
    }

    override fun get(pointer: Heap.Pointer): RuntimeObject =
        data.getOrNull(pointer.value) ?: throw IllegalStateException("Pointer $pointer points to an invalid memory.")

    override fun allocate(literal: ProgramObject): Heap.Pointer {
        val runtimeObject = when (literal) {
            is ProgramObject.Boolean -> RuntimeObject.Boolean(literal.value)
            is ProgramObject.Integer -> RuntimeObject.Integer(literal.value)
            ProgramObject.Null -> return Heap.nullPointer
            else -> throw IllegalStateException("$literal is not a literal and cannot be allocated.")
        }

        return allocate(runtimeObject)
    }

    override fun allocate(runtimeObject: RuntimeObject): Heap.Pointer {
        when (runtimeObject) {
            is RuntimeObject.Null -> {
                return Heap.nullPointer
            }
            is RuntimeObject.Boolean -> {
                return if (runtimeObject.value) truePointer else falsePointer
            }
            is RuntimeObject.Integer -> {
                integerPointer(runtimeObject)?.let { return it }
            }
            else -> {
            }
        }

        val pointer = Heap.Pointer(objectCount)

        data.add(runtimeObject)

        byteSize += runtimeObject.size

        return pointer
    }

    private companion object {

        val falsePointer: Heap.Pointer = Heap.Pointer(1)
        val truePointer: Heap.Pointer = Heap.Pointer(2)

        fun integerPointer(integer: RuntimeObject.Integer): Heap.Pointer? =
            if (integer.value in -128 until 128) {
                Heap.Pointer(integer.value + 128 + 3)
            } else {
                null
            }
    }
}