package com.filipdolnik.cvut.run.vm

import com.filipdolnik.cvut.run.interpreter.Value
import com.filipdolnik.cvut.run.vm.code.Classes
import com.filipdolnik.cvut.run.vm.code.Functions
import com.filipdolnik.cvut.run.vm.code.InstructionPointer
import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.frame.FrameStack
import com.filipdolnik.cvut.run.vm.instruction.Instruction
import com.filipdolnik.cvut.run.vm.memory.GlobalVariables
import com.filipdolnik.cvut.run.vm.memory.heap.Heap
import com.filipdolnik.cvut.run.vm.memory.InstructionArguments
import com.filipdolnik.cvut.run.vm.memory.LocalVariables
import com.filipdolnik.cvut.run.vm.memory.OperandStack
import com.filipdolnik.cvut.run.vm.parser.BytecodeParser
import java.io.Writer

class Environment(
    parsedBytecode: BytecodeParser.ParsedBytecode,
    heapFactory: (FrameStack, GlobalVariables, OperandStack) -> Heap,
    private val stdout: Writer,
) {

    private val code = parsedBytecode.code

    val constantPool: ConstantPool = parsedBytecode.constantPool

    val frameStack: FrameStack = FrameStack()

    val globalVariables: GlobalVariables = GlobalVariables(parsedBytecode.globalVariables, constantPool)
    val localVariables: LocalVariables = LocalVariables(frameStack)
    val operandStack: OperandStack = OperandStack()

    val heap: Heap = heapFactory(frameStack, globalVariables, operandStack)

    val arguments: InstructionArguments = InstructionArguments(this, code)
    val functions: Functions = Functions(parsedBytecode.functions, constantPool)

    val classes: Classes = Classes(constantPool)

    val hasNextInstruction: Boolean
        get() = instructionPointer.value in code.indices

    fun nextInstruction(): Instruction =
        Instruction.from(code[instructionPointer])

    var instructionPointer: InstructionPointer = parsedBytecode.mainFunction.entryPoint
        private set

    init {
        frameStack.push(InstructionPointer(-1), parsedBytecode.mainFunction.totalVariableCount)
    }

    fun execute(instruction: Instruction) {
        instructionPointer = instruction.executeIn(this)
    }

    fun output(value: Value) {
        stdout.write("$value")
    }
}