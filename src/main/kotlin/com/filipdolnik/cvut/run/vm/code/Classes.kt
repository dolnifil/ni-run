package com.filipdolnik.cvut.run.vm.code

import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

class Classes(
    private val constantPool: ConstantPool,
) {

    private val classes = mutableMapOf<ConstantPool.Index, RuntimeObject.Object.Class>()

    operator fun get(index: ConstantPool.Index): RuntimeObject.Object.Class {
        return classes.getOrPut(index) {
            val classProgramObject = constantPool[index] as? ProgramObject.Class
                ?: throw IllegalStateException("Index $index must point to a class. Was ${constantPool[index]}")

            val fieldsNameIndex = classProgramObject.members.mapNotNull {
                (constantPool[it] as? ProgramObject.Slot)?.nameIndex
            }

            val methods = classProgramObject.members.filter {
                constantPool[it] is ProgramObject.Method
            }

            RuntimeObject.Object.Class(fieldsNameIndex, methods)
        }
    }
}