package com.filipdolnik.cvut.run.vm.frame

import com.filipdolnik.cvut.run.vm.code.InstructionPointer

class FrameStack {

    val frames: ArrayDeque<Frame> = ArrayDeque()

    val isEmpty: Boolean
        get() = frames.isEmpty()

    fun pop(): InstructionPointer =
        (frames.removeLastOrNull() ?: throw IllegalStateException("Stack is empty.")).returnAddress

    fun push(pointer: InstructionPointer, localVariableCount: Int) {
        frames.add(Frame(pointer, localVariableCount))
    }

    fun peek(): Frame =
        frames.lastOrNull() ?: throw IllegalStateException("Stack is empty.")
}