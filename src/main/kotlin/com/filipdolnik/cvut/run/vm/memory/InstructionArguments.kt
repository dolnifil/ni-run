package com.filipdolnik.cvut.run.vm.memory

import com.filipdolnik.cvut.run.vm.Environment
import com.filipdolnik.cvut.run.vm.code.Code
import com.filipdolnik.cvut.run.vm.code.InstructionPointer
import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject

class InstructionArguments(
    private val environment: Environment,
    private val code: Code,
) {

    fun constantPoolIndex(offset: Int = 1): ConstantPool.Index =
        ConstantPool.Index(uShortAt(environment.instructionPointer, offset))

    fun localVariableIndex(offset: Int = 1): LocalVariables.Index =
        LocalVariables.Index(uShortAt(environment.instructionPointer, offset))

    fun label(offset: Int = 1): InstructionPointer {
        val labelName = environment.constantPool[constantPoolIndex(offset)]

        check(labelName is ProgramObject.String) { "Label must be a string. Was: $labelName" }

        return code.labels[labelName] ?: throw IllegalStateException("Unknown label $labelName.")
    }

    private fun uShortAt(instructionPointer: InstructionPointer, offset: Int): UShort =
        (code[instructionPointer, offset + 1].toInt().shl(8) or code[instructionPointer, offset].toInt()).toUShort()

    operator fun get(offset: Int = 1): UByte = code[environment.instructionPointer, offset]
}