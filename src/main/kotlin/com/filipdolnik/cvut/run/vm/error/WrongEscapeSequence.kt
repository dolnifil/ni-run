package com.filipdolnik.cvut.run.vm.error

class WrongEscapeSequence(format: String): VMRuntimeError("Format string \"$format\" contains unknown escape sequence.")