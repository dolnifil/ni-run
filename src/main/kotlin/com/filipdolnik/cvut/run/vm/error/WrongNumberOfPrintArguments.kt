package com.filipdolnik.cvut.run.vm.error

import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

class WrongNumberOfPrintArguments(format: String, arguments: List<RuntimeObject>):
    VMRuntimeError("Print called with incorrect number of arguments. Format: \"$format\"." +
        "arguments: \"${arguments.joinToString(", ")}\"}")