package com.filipdolnik.cvut.run.vm.instruction

import com.filipdolnik.cvut.run.interpreter.Value
import com.filipdolnik.cvut.run.vm.Environment
import com.filipdolnik.cvut.run.vm.code.InstructionPointer
import com.filipdolnik.cvut.run.vm.constant.ProgramObject
import com.filipdolnik.cvut.run.vm.error.WrongEscapeSequence
import com.filipdolnik.cvut.run.vm.error.WrongNumberOfPrintArguments
import com.filipdolnik.cvut.run.vm.memory.heap.Heap
import com.filipdolnik.cvut.run.vm.memory.LocalVariables
import com.filipdolnik.cvut.run.vm.memory.RuntimeObject

sealed class Instruction {

    abstract val code: Int

    abstract val length: Int

    fun executeIn(environment: Environment): InstructionPointer = environment.run {
        execute() ?: environment.instructionPointer.bumped(length)
    }

    protected abstract fun Environment.execute(): InstructionPointer?

    object Literal: Instruction() {

        override val code: Int = 0x01

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val literal = constantPool[arguments.constantPoolIndex()]

            val pointer = heap.allocate(literal)

            operandStack.push(pointer)

            return null
        }
    }

    object GetLocal: Instruction() {

        override val code: Int = 0x0A

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val index = arguments.localVariableIndex()

            operandStack.push(localVariables[index])

            return null
        }
    }

    object SetLocal: Instruction() {

        override val code: Int = 0x09

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val index = arguments.localVariableIndex()

            localVariables[index] = operandStack.peek()

            return null
        }
    }

    object GetGlobal: Instruction() {

        override val code: Int = 0x0C

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val nameIndex = arguments.constantPoolIndex()

            operandStack.push(globalVariables[nameIndex])

            return null
        }
    }

    object SetGlobal: Instruction() {

        override val code: Int = 0x0B

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val nameIndex = arguments.constantPoolIndex()

            globalVariables[nameIndex] = operandStack.peek()

            return null
        }
    }

    object FunctionCall: Instruction() {

        override val code: Int = 0x08

        override val length: Int = 4

        override fun Environment.execute(): InstructionPointer {
            val nameIndex = arguments.constantPoolIndex()
            val argumentCount = arguments[3]

            val function = functions[nameIndex]
            check(function.argumentCount.value == argumentCount) {
                "Function ${constantPool[nameIndex]} has different number of arguments. " +
                    "Was: $argumentCount Expected: ${function.argumentCount}"
            }

            frameStack.push(instructionPointer.bumped(length), function.totalVariableCount)

            ((argumentCount.toInt() - 1) downTo 0).forEach {
                localVariables[LocalVariables.Index(it.toUShort())] = operandStack.pop()
            }

            return function.entryPoint
        }
    }

    object Return: Instruction() {

        override val code: Int = 0x0F

        override val length: Int = 1

        override fun Environment.execute(): InstructionPointer {
            return frameStack.pop()
        }
    }

    object Jump: Instruction() {

        override val code: Int = 0x0E

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer {
            return arguments.label()
        }
    }

    object Branch: Instruction() {

        override val code: Int = 0x0D

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val pointer = operandStack.pop()

            val runtimeObject = heap[pointer]

            return if (runtimeObject.convertToBoolean(heap)) arguments.label() else null
        }
    }

    object Print: Instruction() {

        override val code: Int = 0x02

        override val length: Int = 4

        override fun Environment.execute(): InstructionPointer? {
            val formatObject = constantPool[arguments.constantPoolIndex()]
            check(formatObject is ProgramObject.String) { "Format of print must be a string. Was: $formatObject" }
            val format = formatObject.value

            val argumentsCount = arguments[3]

            val output = StringBuilder()
            var escaped = false

            val reversedArguments = (0 until argumentsCount.toInt())
                .map { heap[operandStack.pop()] }
                .toMutableList()

            val allArguments = reversedArguments.reversed()

            format.forEach {
                if (escaped) {
                    val character = when (it) {
                        '~' -> '~'
                        'n' -> '\n'
                        '"' -> '"'
                        'r' -> '\r'
                        't' -> '\t'
                        '\\' -> '\\'
                        else -> throw WrongEscapeSequence(format)
                    }
                    output.append(character)

                    escaped = false
                } else {
                    when (it) {
                        '\\' -> escaped = true
                        '~' -> {
                            val argument = reversedArguments.removeLastOrNull() ?: throw WrongNumberOfPrintArguments(format, allArguments)

                            output.append(argument.toString(heap, constantPool))
                        }
                        else -> {
                            output.append(it)
                        }
                    }
                }
            }

            if (escaped) {
                throw WrongEscapeSequence(format)
            }
            if (reversedArguments.isNotEmpty()) {
                throw WrongNumberOfPrintArguments(format, allArguments)
            }

            output(Value.String(output.toString()))

            operandStack.push(Heap.nullPointer)

            return null
        }
    }

    object Array: Instruction() {

        override val code: Int = 0x03

        override val length: Int = 1

        override fun Environment.execute(): InstructionPointer? {
            val initialValuePointer = operandStack.pop()
            val sizePointer = operandStack.pop()

            val size = heap[sizePointer].asIntegerOrNull(heap)
                ?: throw IllegalStateException("Array size must be an integer. Was: ${heap[sizePointer]}")
            check(size.value >= 0) { "Array size must be >= 0. Was $size." }

            val array = Array(size.value) { initialValuePointer }

            val arrayPointer = heap.allocate(RuntimeObject.Array(array))

            operandStack.push(arrayPointer)

            return null
        }
    }

    object Object: Instruction() {

        override val code: Int = 0x04

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val classIndex = arguments.constantPoolIndex()

            val classObject = classes[classIndex]

            val fields = classObject.fieldsNameIndex
                .map {
                    operandStack.pop()
                }
                .reversed()
                .toTypedArray()

            val parent = operandStack.pop()

            val runtimeObjectPointer = heap.allocate(RuntimeObject.Object(parent, classObject, fields))

            operandStack.push(runtimeObjectPointer)

            return null
        }
    }

    object GetField: Instruction() {

        override val code: Int = 0x05

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val nameIndex = arguments.constantPoolIndex()

            val fieldName = constantPool[nameIndex] as ProgramObject.String

            val receiverPointer = operandStack.pop()
            val receiver = heap[receiverPointer] as RuntimeObject.Object

            val fieldIndex = receiver.type.fieldsNameIndex.indexOfFirst {
                constantPool[it] == fieldName
            }

            val fieldValuePointer = receiver.fields[fieldIndex]

            operandStack.push(fieldValuePointer)

            return null
        }
    }

    object SetField: Instruction() {

        override val code: Int = 0x06

        override val length: Int = 3

        override fun Environment.execute(): InstructionPointer? {
            val nameIndex = arguments.constantPoolIndex()

            val fieldName = constantPool[nameIndex] as ProgramObject.String

            val setValuePointer = operandStack.pop()

            val receiverPointer = operandStack.pop()
            val receiver = heap[receiverPointer] as RuntimeObject.Object

            val fieldIndex = receiver.type.fieldsNameIndex.indexOfFirst {
                constantPool[it] == fieldName
            }

            receiver.fields[fieldIndex] = setValuePointer

            operandStack.push(setValuePointer)

            return null
        }
    }

    object MethodCall: Instruction() {

        override val code: Int = 0x07

        override val length: Int = 4

        override fun Environment.execute(): InstructionPointer? {
            val nameIndex = arguments.constantPoolIndex()
            val argumentCount = arguments[3]

            val methodName = constantPool[nameIndex] as ProgramObject.String
            val arguments = mutableListOf<Heap.Pointer>()

            repeat((argumentCount - 1u).toInt()) {
                arguments.add(operandStack.pop())
            }
            arguments.reverse()

            val receiverPointer = operandStack.pop()
            var receiver = heap[receiverPointer]

            while (true) {
                if (receiver is RuntimeObject.Object) {
                    val method = receiver.type.methods
                        .firstOrNull {
                            val method = constantPool[it] as ProgramObject.Method

                            constantPool[method.nameIndex] == methodName
                        }
                        ?.let { constantPool[it] as ProgramObject.Method }

                    if (method != null) {
                        frameStack.push(instructionPointer.bumped(length), method.totalVariableCount + 1)

                        localVariables[LocalVariables.Index(0u)] = receiverPointer
                        arguments.forEachIndexed { index, argument ->
                            localVariables[LocalVariables.Index((index + 1).toUShort())] = argument
                        }

                        return method.entryPoint
                    } else {
                        receiver = heap[receiver.parent]
                    }
                } else {
                    val builtInMethods = BuiltInMethods.resolve(receiver)
                        ?: throw IllegalStateException("RuntimeObject $receiver cannot be a receiver.")

                    builtInMethods.executeIn(this, receiver, methodName, arguments)

                    return null
                }
            }
        }
    }

    object Drop: Instruction() {

        override val code: Int = 0x10

        override val length: Int = 1

        override fun Environment.execute(): InstructionPointer? {
            operandStack.pop()

            return null
        }
    }

    companion object {

        private fun all(): List<Instruction> =
            Instruction::class.sealedSubclasses.map {
                it.objectInstance ?: throw IllegalStateException("All instructions must be objects: $it")
            }

        private val instructionMap = run {
            val instructions = all()
            val instructionByCode = instructions.associateBy { it.code }

            (0 until 255).map { instructionByCode[it] }
        }

        fun from(instructionCode: UByte): Instruction =
            instructionMap[instructionCode.toInt()] ?: throw IllegalStateException("Instruction code $instructionCode is not supported.")
    }
}