package com.filipdolnik.cvut.run.vm.memory

import com.filipdolnik.cvut.run.vm.frame.FrameStack
import com.filipdolnik.cvut.run.vm.memory.heap.Heap

class LocalVariables(
    private val frameStack: FrameStack,
) {

    operator fun get(index: Index): Heap.Pointer =
        frameStack.peek()[index]

    operator fun set(index: Index, pointer: Heap.Pointer) {
        frameStack.peek()[index] = pointer
    }

    inline class Index(val value: UShort)
}
