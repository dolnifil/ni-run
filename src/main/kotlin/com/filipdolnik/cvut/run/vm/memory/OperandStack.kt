package com.filipdolnik.cvut.run.vm.memory

import com.filipdolnik.cvut.run.vm.memory.heap.Heap

class OperandStack {

    val data: ArrayDeque<Heap.Pointer> = ArrayDeque()

    fun pop(): Heap.Pointer =
        data.removeLastOrNull() ?: throw IllegalStateException("Stack is empty.")

    fun push(pointer: Heap.Pointer) {
        data.add(pointer)
    }

    fun peek(): Heap.Pointer =
        data.lastOrNull() ?: throw IllegalStateException("Stack is empty.")
}