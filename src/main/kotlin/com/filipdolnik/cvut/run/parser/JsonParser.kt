package com.filipdolnik.cvut.run.parser

import com.filipdolnik.cvut.run.ast.AstNode
import com.filipdolnik.cvut.run.ast.Identifier
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

class JsonParser {

    fun parse(json: String): AstNode = Json.parseToJsonElement(json).convertToAstNode()

    private fun JsonElement.convertToAstNode(): AstNode = when (this) {
        is JsonObject -> {
            if (keys.size != 1) {
                throw IllegalArgumentException("AST JSON node is expected to have exactly one key. $this")
            }
            val key = entries.first().key
            val value = entries.first().value

            when (key) {
                "Integer" -> AstNode.Integer(value.convertToInt())
                "Boolean" -> AstNode.Boolean(value.convertToBoolean())
                "Variable" -> AstNode.VariableDefinition(value.getIdentifier("name"), value.getAstNode("value"))
                "AccessVariable" -> AstNode.VariableAccess(value.getIdentifier("name"))
                "AssignVariable" -> AstNode.VariableAssignment(value.getIdentifier("name"), value.getAstNode("value"))
                "Array" -> AstNode.Array(value.getAstNode("size"), value.getAstNode("value"))
                "AccessArray" -> AstNode.ArrayAccess(value.getAstNode("array"), value.getAstNode("index"))
                "AssignArray" -> AstNode.ArrayAssignment(
                    value.getAstNode("array"),
                    value.getAstNode("index"),
                    value.getAstNode("value"),
                )
                "Function" -> AstNode.Function(
                    value.getIdentifier("name"),
                    value.getIdentifiers("parameters"),
                    value.getAstNode("body"),
                )
                "CallFunction" -> AstNode.FunctionCall(value.getIdentifier("name"), value.getAstNodes("arguments"))
                "Print" -> AstNode.Print(value.getString("format"), value.getAstNodes("arguments"))
                "Block" -> AstNode.Block(getAstNodes("Block"))
                "Top" -> AstNode.Top(getAstNodes("Top"))
                "Loop" -> AstNode.While(value.getAstNode("condition"), value.getAstNode("body"))
                "Conditional" -> AstNode.If(
                    value.getAstNode("condition"),
                    value.getAstNode("consequent"),
                    value.getAstNode("alternative"),
                )
                "Object" -> AstNode.Object(value.getAstNode("extends"), value.getAstNodes("members"))
                "AssignField" -> AstNode.FieldAssignment(
                    value.getAstNode("object"),
                    value.getIdentifier("field"),
                    value.getAstNode("value"),
                )
                "AccessField" -> AstNode.FieldAccess(value.getAstNode("object"), value.getIdentifier("field"))
                "CallMethod" -> AstNode.MethodCall(
                    value.getAstNode("object"),
                    value.getIdentifier("name"),
                    value.getAstNodes("arguments"),
                )
                else -> throw IllegalArgumentException("Unknown AST element $this.")
            }
        }
        is JsonPrimitive -> if (content == "Null") AstNode.Null else throw IllegalArgumentException("Unknown AST element $this.")
        else -> throw IllegalArgumentException("Unknown AST element $this.")
    }

    private fun JsonElement.convertToString(): String = if (jsonPrimitive.isString) {
        jsonPrimitive.content
    } else {
        throw IllegalArgumentException("JSON element $this is not a String.")
    }

    private fun JsonElement.convertToInt(): Int = jsonPrimitive.content.toInt()

    private fun JsonElement.convertToBoolean(): Boolean = jsonPrimitive.content.toBoolean()

    private fun JsonElement.getString(name: String): String = jsonObject.getValue(name).convertToString()

    private fun JsonElement.getIdentifier(name: String): Identifier = Identifier(getString(name))

    private fun JsonElement.getIdentifiers(name: String): List<Identifier> =
        jsonObject.getValue(name).jsonArray.map { Identifier(it.convertToString()) }

    private fun JsonElement.getAstNode(name: String): AstNode = jsonObject.getValue(name).convertToAstNode()

    private fun JsonElement.getAstNodes(name: String): List<AstNode> = jsonObject.getValue(name).jsonArray.map { it.convertToAstNode() }
}