package com.filipdolnik.cvut.run

import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.readBytes

@ExperimentalPathApi
fun main(args: Array<String>) {
    if (args.size < 2) {
        throw IllegalArgumentException(
            "Must have at least two arguments. Command (run, compile, execute) and a file path.\n" +
                "Options:\n" +
                "--heap-size [MB]\n" +
                "--heap-log [file]"
        )
    }

    val command = args[0].toLowerCase()
    var heapSize: Int? = null
    var heapLog: String? = null
    var input: String? = null

    var index = 1
    while (index in args.indices) {
        when (val argument = args[index++]) {
            "--heap-size" -> {
                require(index < args.lastIndex) { "--heap-size must be followed by an Int." }
                heapSize = args[index++].toIntOrNull() ?: throw IllegalArgumentException("--heap-size must be an Int.")
            }
            "--heap-log" -> {
                require(index < args.lastIndex) { "--heap-log must be followed by an file path." }
                heapLog = args[index++]
            }
            else -> {
                if (index == args.size) {
                    input = argument
                } else {
                    throw IllegalArgumentException("Incorrect number of arguments. File path must be last argument.")
                }
            }
        }
    }

    requireNotNull(input) { "Last parameter must be an input file path." }

    when (command) {
        "ast" -> {
            runAst(input)
        }
        "run" -> {
            val bytecode = compile(input)
            val result = run(bytecode, heapSize, heapLog)
            print(result)
        }
        "compile" -> {
            val result = compile(input)
            System.out.write(result)
        }
        "execute" -> {
            val bytecode = Path(input).readBytes()
            val result = run(bytecode, heapSize, heapLog)
            print(result)
        }
        else -> throw IllegalArgumentException("Unknown command. Must be (run, compile, execute).")
    }
}