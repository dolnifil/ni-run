package com.filipdolnik.cvut.run

import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
fun main(args: Array<String>) {
    if (args.size != 1) {
        throw IllegalArgumentException("Must have exactly one argument with path to a file with code.")
    }

    val bytecode = compile(args[0])
    val result = run(bytecode, null, null)
    print(result)
}