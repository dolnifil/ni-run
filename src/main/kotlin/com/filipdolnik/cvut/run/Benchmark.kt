package com.filipdolnik.cvut.run

import kotlin.io.path.ExperimentalPathApi
import kotlin.system.measureTimeMillis

@ExperimentalPathApi
fun main(args: Array<String>) {
    if (args.size != 2) {
        throw IllegalArgumentException("Must have exactly two arguments. Command (run, compile, execute) and a file path.")
    }

    val bytecode = compile(args[1])

    repeat(10) {
        run(bytecode, null, null)
    }

    println(measureTimeMillis {
        run(bytecode, null, null)
    })
}