package com.filipdolnik.cvut.run

import com.filipdolnik.cvut.run.vm.VM
import com.filipdolnik.cvut.run.vm.parser.BytecodeParser
import java.io.StringWriter
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.readBytes

@ExperimentalPathApi
fun main(args: Array<String>) {
    if (args.size != 1) {
        throw IllegalArgumentException("Must have exactly one argument with path to a file with bytecode.")
    }

    val bytecode = Path(args[0]).readBytes()
    val result = run(bytecode, null, null)
    print(result)
}

fun run(bytecode: ByteArray, heapSize: Int?, heapLog: String?): String {
    val parsedBytecode = BytecodeParser().parse(bytecode.toUByteArray())
    val vm = VM(parsedBytecode)

    StringWriter().use { writer ->
        vm.run(writer, heapSize, heapLog)

        return writer.toString()
    }
}