package com.filipdolnik.cvut.run

import com.filipdolnik.cvut.run.interpreter.Interpreter
import com.filipdolnik.cvut.run.parser.JsonParser
import java.io.StringWriter
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.exists

@ExperimentalPathApi
fun main() {
    val json = StringBuilder()

    var line = readLine()
    while (line != null) {
        json.append(line)
        line = readLine()
    }

    val ast = JsonParser().parse(json.toString())

    val interpreter = Interpreter()

    StringWriter().use { writer ->
        interpreter.run(ast, writer)

        print(writer.toString())
    }
}

@ExperimentalPathApi
fun runAst(file: String) {
    val path = if (Path("FML/target/release/fml.exe").exists()) {
        Path("FML/target/release/fml.exe")
    } else {
        Path("FML/target/release/fml")
    }.toFile().canonicalPath

    val process = ProcessBuilder(path, "parse", Path(file).toFile().canonicalPath, "--format=JSON")
        .start()
    val json = process.inputStream.readAllBytes().decodeToString()
    process.waitFor()
    val error = process.errorStream.readAllBytes().decodeToString()

    if (error.isNotBlank()) {
        throw IllegalStateException(error)
    } else {
        val ast = JsonParser().parse(json)

        val interpreter = Interpreter()

        StringWriter().use { writer ->
            interpreter.run(ast, writer)

            print(writer.toString())
        }
    }
}