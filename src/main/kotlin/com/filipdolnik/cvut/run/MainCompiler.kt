package com.filipdolnik.cvut.run

import com.filipdolnik.cvut.run.compiler.Compiler
import com.filipdolnik.cvut.run.parser.JsonParser
import com.filipdolnik.cvut.run.vm.VM
import com.filipdolnik.cvut.run.vm.parser.BytecodeParser
import java.io.StringWriter
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.exists

@ExperimentalPathApi
fun main(args: Array<String>) {
    if (args.size != 1) {
        throw IllegalArgumentException("Must have exactly one argument with path to a file with code.")
    }

    val result = compile(args[0])
    System.out.write(result)
}

@ExperimentalPathApi
fun compile(file: String): ByteArray {
    val path = if (Path("FML/target/release/fml.exe").exists()) {
        Path("FML/target/release/fml.exe")
    } else {
        Path("FML/target/release/fml")
    }.toFile().canonicalPath

    val process = ProcessBuilder(path, "parse", Path(file).toFile().canonicalPath, "--format=JSON")
        .start()
    val json = process.inputStream.readAllBytes().decodeToString()
    process.waitFor()
    val error = process.errorStream.readAllBytes().decodeToString()

    if (error.isNotBlank()) {
        throw IllegalStateException(error)
    } else {
        val ast = JsonParser().parse(json)

        val compiler = Compiler(ast)

        val bytecode = compiler.compile()

        return bytecode.toByteArray()
    }
}