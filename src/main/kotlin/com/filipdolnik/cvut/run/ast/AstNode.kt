package com.filipdolnik.cvut.run.ast

sealed interface AstNode {

    data class Integer(val value: Int): AstNode

    data class Boolean(val value: kotlin.Boolean): AstNode

    object Null: AstNode

    data class VariableDefinition(val name: Identifier, val value: AstNode): AstNode

    data class VariableAccess(val name: Identifier): AstNode

    data class VariableAssignment(val name: Identifier, val value: AstNode): AstNode

    data class Array(val size: AstNode, val value: AstNode): AstNode

    data class ArrayAccess(val array: AstNode, val index: AstNode): AstNode

    data class ArrayAssignment(val array: AstNode, val index: AstNode, val value: AstNode): AstNode

    data class Function(val name: Identifier, val parameters: List<Identifier>, val body: AstNode): AstNode

    data class FunctionCall(val name: Identifier, val arguments: List<AstNode>): AstNode

    data class Print(val format: String, val arguments: List<AstNode>): AstNode

    data class Block(val body: List<AstNode>): AstNode

    data class Top(val body: List<AstNode>): AstNode

    data class While(val condition: AstNode, val body: AstNode): AstNode

    data class If(val condition: AstNode, val thenBranch: AstNode, val elseBranch: AstNode): AstNode

    data class Object(val parent: AstNode, val body: List<AstNode>): AstNode

    data class FieldAssignment(val receiver: AstNode, val field: Identifier, val value: AstNode): AstNode

    data class FieldAccess(val receiver: AstNode, val field: Identifier): AstNode

    data class MethodCall(val receiver: AstNode, val name: Identifier, val arguments: List<AstNode>): AstNode
}
