package com.filipdolnik.cvut.run.ast

inline class Identifier(val name: String)