package com.filipdolnik.cvut.run.compiler.ir

import com.filipdolnik.cvut.run.ast.Identifier

class Function(
    val identifier: Identifier?,
    val parameters: List<Variable.Local>,
    val localVariables: List<Variable.Local>,
    val entryPoint: BasicBlock,
)