package com.filipdolnik.cvut.run.compiler.ir

class Program(val globalVariables: List<Variable.Global>, val functions: List<Function>, val entryPoint: Function)