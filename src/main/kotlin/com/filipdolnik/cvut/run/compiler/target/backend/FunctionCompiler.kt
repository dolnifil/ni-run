package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.compiler.ir.BasicBlock
import com.filipdolnik.cvut.run.compiler.ir.Function
import com.filipdolnik.cvut.run.compiler.ir.Instruction
import com.filipdolnik.cvut.run.compiler.target.VMFunction
import com.filipdolnik.cvut.run.compiler.target.VMInstruction

class FunctionCompiler(
    private val function: Function,
    globalVariableCache: GlobalVariableCache,
    private val functionCache: FunctionCache,
    private val labelCache: LabelCache,
    private val isMainFunction: Boolean = false,
) {

    private val variableCache = VariableCache(globalVariableCache, function)
    private val registerCache = RegisterCache(variableCache)

    fun compile(): VMFunction {
        val vmFunction = functionCache.getOrCreate(function)
        vmFunction.parameters.clear()
        vmFunction.localVariables.clear()
        vmFunction.code.clear()

        val parents = mutableMapOf<BasicBlock, MutableSet<BasicBlock>>()
        val children = mutableMapOf<BasicBlock, MutableSet<BasicBlock>>()

        val basicBlocks = basicBlocksInOrder(parents, children)

        val basicBlockWithCompilers = basicBlocks.associateWith {
            BasicBlockCompiler(it, it == function.entryPoint, variableCache, registerCache, labelCache, functionCache, isMainFunction)
        }

        basicBlockWithCompilers.forEach { it.value.prepare() }

        val basicBlocksWithResult = basicBlocksWithResult(parents, children, basicBlockWithCompilers)
        val basicBlocksWithCode = basicBlocks
            .map { it to basicBlockWithCompilers[it]!! }
            .map { it.first to it.second.compile(it.first in basicBlocksWithResult) }

        val optimizedBasicBlocksWithCode = removeUnusedBasicBlocks(inlineBasicBlocks(basicBlocksWithCode))

        val code = optimizedBasicBlocksWithCode.flatMap { it.second }
        val optimizedCode = removeUnusedLabels(optimizeJumps(code))

        vmFunction.parameters.addAll(variableCache.parameters)
        vmFunction.localVariables.addAll(variableCache.localVariables)
        vmFunction.code.addAll(optimizedCode)

        return vmFunction
    }

    private fun basicBlocksInOrder(
        parents: MutableMap<BasicBlock, MutableSet<BasicBlock>>,
        children: MutableMap<BasicBlock, MutableSet<BasicBlock>>,
    ): List<BasicBlock> {
        val foundBasicBlocks = mutableSetOf<BasicBlock>()
        val orderedBasicBlocks = mutableListOf<BasicBlock>()
        val nonVisitedBasicBlocks = mutableListOf(function.entryPoint)

        while (nonVisitedBasicBlocks.isNotEmpty()) {
            val basicBlock = nonVisitedBasicBlocks.removeLast()
            if (!foundBasicBlocks.add(basicBlock)) {
                continue
            }
            orderedBasicBlocks.add(basicBlock)

            when (val lastInstruction = basicBlock.instructions.lastOrNull()) {
                is Instruction.ConditionalJump -> {
                    nonVisitedBasicBlocks.add(lastInstruction.thenBranch)
                    nonVisitedBasicBlocks.add(lastInstruction.elseBranch)

                    parents.getOrPut(lastInstruction.thenBranch) { mutableSetOf() }.add(basicBlock)
                    children.getOrPut(basicBlock) { mutableSetOf() }.add(lastInstruction.thenBranch)

                    parents.getOrPut(lastInstruction.elseBranch) { mutableSetOf() }.add(basicBlock)
                    children.getOrPut(basicBlock) { mutableSetOf() }.add(lastInstruction.elseBranch)
                }
                is Instruction.Jump -> {
                    nonVisitedBasicBlocks.add(lastInstruction.basicBlock)

                    parents.getOrPut(lastInstruction.basicBlock) { mutableSetOf() }.add(basicBlock)
                    children.getOrPut(basicBlock) { mutableSetOf() }.add(lastInstruction.basicBlock)
                }
                else -> {
                }
            }
        }

        return orderedBasicBlocks.filter { it.isTerminated } + orderedBasicBlocks.filter { !it.isTerminated }
    }

    private fun basicBlocksWithResult(
        parents: Map<BasicBlock, MutableSet<BasicBlock>>,
        children: Map<BasicBlock, MutableSet<BasicBlock>>,
        basicBlockWithCompilers: Map<BasicBlock, BasicBlockCompiler>,
    ): Set<BasicBlock> {
        val basicBlocksWithResult = parents
            .filter { basicBlockWithCompilers[it.key]?.needsPreviousBasicBlockValue == true }
            .flatMap { it.value }
            .toSet()

        basicBlocksWithResult.forEach { basicBlock ->
            val allChildrenNeedsResult =
                children[basicBlock]?.all { basicBlockWithCompilers[it]?.needsPreviousBasicBlockValue == true } ?: false
            if (!allChildrenNeedsResult) {
                throw IllegalStateException("Some child of $basicBlock does not need result while other do.")
            }
        }

        return basicBlocksWithResult
    }

    private fun inlineBasicBlocks(
        basicBlocksWithCode: List<Pair<BasicBlock, List<VMInstruction>>>,
    ): List<Pair<BasicBlock, List<VMInstruction>>> {
        var shouldContinue = true
        var result = basicBlocksWithCode

        while (shouldContinue) {
            val inliningResult = inlineBasicBlocksNonRepeating(result)
            result = inliningResult.first
            shouldContinue = inliningResult.second
        }

        return result
    }

    private fun inlineBasicBlocksNonRepeating(
        basicBlocksWithCode: List<Pair<BasicBlock, List<VMInstruction>>>,
    ): Pair<List<Pair<BasicBlock, List<VMInstruction>>>, Boolean> {
        val inlinedBasicBlocksWithCode = basicBlocksWithCode.drop(1).filter { it.second.size <= 2 && it.first.isTerminated }
        val inlinedLabelsWithCode = inlinedBasicBlocksWithCode.mapNotNull { (_, instructions) ->
            (instructions.firstOrNull() as? VMInstruction.Label)?.let { it to instructions }
        }.toMap()

        var somethingWasInlined = false

        return basicBlocksWithCode.map {
            val lastInstructionLabel = when (val lastInstruction = it.second.lastOrNull()) {
                is VMInstruction.Branch -> lastInstruction.thenLabel
                is VMInstruction.Jump -> lastInstruction.label
                else -> null
            }
            val inlinedCode = inlinedLabelsWithCode[lastInstructionLabel]
            it.first to if (inlinedCode != null) {
                somethingWasInlined = true
                it.second.dropLast(1) + inlinedCode.drop(1)
            } else {
                it.second
            }
        } to somethingWasInlined
    }

    private fun removeUnusedBasicBlocks(
        basicBlocksWithCode: List<Pair<BasicBlock, List<VMInstruction>>>,
    ): List<Pair<BasicBlock, List<VMInstruction>>> {
        val unusedBasicBlockByLabel = basicBlocksWithCode.mapNotNull { (basicBlock, instructions) ->
            (instructions.firstOrNull() as? VMInstruction.Label)?.let { it to basicBlock }
        }.toMap().toMutableMap()

        basicBlocksWithCode.forEach { (_, instructions) ->
            instructions.forEach {
                when (it) {
                    is VMInstruction.Branch -> unusedBasicBlockByLabel.remove(it.thenLabel)
                    is VMInstruction.Jump -> unusedBasicBlockByLabel.remove(it.label)
                    else -> {
                    }
                }
            }
        }

        val unusedBasicBlocks = unusedBasicBlockByLabel.values.toSet()
        return basicBlocksWithCode.filter { it.first !in unusedBasicBlocks }
    }

    private fun optimizeJumps(instructions: List<VMInstruction>): List<VMInstruction> {
        val removedJumps = instructions.zipWithNext().mapNotNull { (jump, label) ->
            if (jump is VMInstruction.Jump && label is VMInstruction.Label && jump.label == label) {
                null
            } else {
                jump
            }
        } + instructions.lastOrNull()

        val usedLabels = mutableSetOf<VMInstruction.Label>()

        removedJumps.forEach {
            when (it) {
                is VMInstruction.Branch -> usedLabels.add(it.thenLabel)
                is VMInstruction.Jump -> usedLabels.add(it.label)
                else -> {
                }
            }
        }

        return removedJumps.mapNotNull {
            if (it is VMInstruction.Label && it !in usedLabels) {
                null
            } else {
                it
            }
        }
    }

    private fun removeUnusedLabels(instructions: List<VMInstruction>): List<VMInstruction> {
        val unusedLabels = instructions.filterIsInstance<VMInstruction.Label>().toMutableSet()
        instructions.forEach {
            when (it) {
                is VMInstruction.Branch -> unusedLabels.remove(it.thenLabel)
                is VMInstruction.Jump -> unusedLabels.remove(it.label)
                else -> {
                }
            }
        }
        return instructions.filter { it !in unusedLabels }
    }
}