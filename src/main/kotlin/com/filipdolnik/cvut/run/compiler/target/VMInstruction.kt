package com.filipdolnik.cvut.run.compiler.target

import com.filipdolnik.cvut.run.ast.Identifier

sealed interface VMInstruction {

    sealed interface Literal: VMInstruction {

        data class Integer(val value: Int): Literal

        data class Boolean(val value: kotlin.Boolean): Literal

        object Null: Literal
    }

    data class GetLocal(val variable: VMVariable.Local): VMInstruction

    data class SetLocal(val variable: VMVariable.Local): VMInstruction

    data class GetGlobal(val variable: VMVariable.Global): VMInstruction

    data class SetGlobal(val variable: VMVariable.Global): VMInstruction

    data class FunctionCall(val function: VMFunction): VMInstruction

    object Return: VMInstruction

    data class Label(val name: Identifier): VMInstruction

    data class Jump(val label: Label): VMInstruction

    data class Branch(val thenLabel: Label): VMInstruction

    data class Print(val format: String, val parameterCount: Int): VMInstruction

    object Array: VMInstruction

    data class Object(val fields: List<Identifier>, val methods: List<VMFunction>): VMInstruction

    data class GetField(val name: Identifier): VMInstruction

    data class SetField(val name: Identifier): VMInstruction

    data class MethodCall(val name: Identifier, val parameterCount: Int): VMInstruction

    object Drop: VMInstruction
}