package com.filipdolnik.cvut.run.compiler.target

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.Variable

class VMFunction(
    val identifier: Identifier,
    val parameters: MutableList<VMVariable.Local>,
    val localVariables: MutableList<VMVariable.Local>,
    val code: MutableList<VMInstruction>,
)