package com.filipdolnik.cvut.run.compiler

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.Variable

abstract class Scope {

    protected val children: MutableList<Nested> = mutableListOf()

    operator fun get(identifier: Identifier): Variable =
        getOrNull(identifier) ?: throw IllegalStateException("Unknown identifier $identifier.")

    fun nested(): Scope = Nested(this).also { children.add(it) }

    abstract fun declareVariable(identifier: Identifier?): Variable

    protected abstract fun getOrNull(identifier: Identifier): Variable?

    class Global: Scope() {

        val globalVariables: List<Variable.Global>
            get() = mutableGlobalVariables

        val localVariables: List<Variable.Local>
            get() = children.flatMap { it.localVariables }

        private val mutableGlobalVariables = mutableListOf<Variable.Global>()
        private val map = mutableMapOf<Identifier, Variable.Global>()

        override fun declareVariable(identifier: Identifier?): Variable {
            val variable = Variable.Global(identifier)

            if (identifier != null) {
                if (identifier in map) {
                    throw IllegalStateException("Identifier $identifier is already declared.")
                }
                map[identifier] = variable
            }
            mutableGlobalVariables.add(variable)

            return variable
        }

        override fun getOrNull(identifier: Identifier): Variable? = map[identifier]
    }

    class Function(parentScope: Scope, parameters: List<Identifier>): Nested(parentScope) {

        val parameters: List<Variable.Local> = parameters.map { Variable.Local(it) }

        init {
            this.parameters.forEach {
                if (it.identifier != null) {
                    map[it.identifier] = it
                }
            }
        }
    }

    open class Nested(private val parentScope: Scope): Scope() {

        val localVariables: List<Variable.Local>
            get() = currentScopeLocalVariables + children.flatMap { it.localVariables }

        protected val map: MutableMap<Identifier, Variable.Local> = mutableMapOf()

        private val currentScopeLocalVariables = mutableListOf<Variable.Local>()

        override fun getOrNull(identifier: Identifier): Variable? =
            map[identifier] ?: parentScope.getOrNull(identifier)

        override fun declareVariable(identifier: Identifier?): Variable {
            val variable = Variable.Local(identifier)

            if (identifier != null) {
                if (identifier in map) {
                    throw IllegalStateException("Identifier $identifier is already declared.")
                }
                map[identifier] = variable
            }
            currentScopeLocalVariables.add(variable)

            return variable
        }
    }
}