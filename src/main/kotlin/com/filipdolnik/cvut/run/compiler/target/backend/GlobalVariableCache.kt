package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.Program
import com.filipdolnik.cvut.run.compiler.ir.Variable
import com.filipdolnik.cvut.run.compiler.target.VMVariable

class GlobalVariableCache(program: Program) {

    private val map = mutableMapOf<Variable.Global, VMVariable.Global>()

    init {
        val usedIdentifiers = program.globalVariables.mapNotNull { it.identifier }

        var nextId = 0

        program.globalVariables.forEach {
            val identifier = it.identifier ?: run {
                val (identifier, usedId) = getUniqueIdentifier(usedIdentifiers, nextId)
                nextId = usedId + 1
                identifier
            }

            map[it] = VMVariable.Global(identifier)
        }
    }

    operator fun get(variable: Variable.Global): VMVariable.Global =
        map[variable] ?: throw IllegalArgumentException("Variable $variable was not registered as a global.")

    private fun getUniqueIdentifier(usedIdentifiers: List<Identifier>, nextId: Int): Pair<Identifier, Int> {
        var i = nextId
        while (true) {
            val candidate = Identifier("g_$i")
            if (candidate !in usedIdentifiers) {
                return candidate to i
            }
            i++
        }
    }
}