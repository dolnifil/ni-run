package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.BasicBlock
import com.filipdolnik.cvut.run.compiler.target.VMInstruction

class LabelCache {

    private val labels = mutableMapOf<BasicBlock, VMInstruction.Label>()

    private var nextId = 0

    operator fun get(basicBlock: BasicBlock): VMInstruction.Label = labels.getOrPut(basicBlock) {
        VMInstruction.Label(Identifier("l_${nextId++}"))
    }
}