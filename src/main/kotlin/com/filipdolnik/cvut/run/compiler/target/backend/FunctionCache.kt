package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.Function
import com.filipdolnik.cvut.run.compiler.ir.Program
import com.filipdolnik.cvut.run.compiler.target.VMFunction

class FunctionCache(program: Program) {

    private val identifierMap = mutableMapOf<Identifier, VMFunction>()
    private val functionMap = mutableMapOf<Function, VMFunction>()

    private var nextId = 0
    private val usedIdentifiers = (program.functions + program.entryPoint).mapNotNull { it.identifier }

    init {
        (program.functions + program.entryPoint).forEach {
            getOrCreate(it)
        }
    }

    operator fun get(function: Function): VMFunction =
        functionMap[function] ?: throw IllegalArgumentException("Unknown function $function.")

    fun getOrCreate(function: Function): VMFunction =
        functionMap.getOrPut(function) {
            val identifier = function.identifier ?: run {
                val (identifier, usedId) = getUniqueIdentifier(usedIdentifiers, nextId)
                nextId = usedId + 1
                identifier
            }

            val vmFunction = VMFunction(identifier, mutableListOf(), mutableListOf(), mutableListOf())
            identifierMap[identifier] = vmFunction
            vmFunction
        }

    operator fun get(name: Identifier): VMFunction =
        identifierMap[name] ?: throw IllegalArgumentException("Unknown function $name.")

    private fun getUniqueIdentifier(usedIdentifiers: List<Identifier>, nextId: Int): Pair<Identifier, Int> {
        var i = nextId
        while (true) {
            val candidate = Identifier("f_$i")
            if (candidate !in usedIdentifiers) {
                return candidate to i
            }
            i++
        }
    }
}