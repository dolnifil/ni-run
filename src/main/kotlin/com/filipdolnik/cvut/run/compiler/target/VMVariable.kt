package com.filipdolnik.cvut.run.compiler.target

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.Instruction
import com.filipdolnik.cvut.run.compiler.ir.Variable

sealed interface VMVariable {

    data class Global(val identifier: Identifier): VMVariable

    data class Local(val index: UShort): VMVariable
}
