package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.compiler.ir.BasicBlock
import com.filipdolnik.cvut.run.compiler.ir.Instruction
import com.filipdolnik.cvut.run.compiler.ir.Value
import com.filipdolnik.cvut.run.compiler.ir.Variable
import com.filipdolnik.cvut.run.compiler.target.VMInstruction
import com.filipdolnik.cvut.run.compiler.target.VMVariable
import kotlin.math.min

class BasicBlockCompiler(
    private val basicBlock: BasicBlock,
    private val isEntryPoint: Boolean,
    private val variableCache: VariableCache,
    private val registerCache: RegisterCache,
    private val labelCache: LabelCache,
    private val functionCache: FunctionCache,
    private val keepStack: Boolean,
) {

    private val instructions = mutableListOf<InstructionWithRegister>()

    private val stack = mutableListOf<Pair<Value, InstructionWithRegister>>()
    private val variableMapping = mutableMapOf<Variable, Value>()

    var needsPreviousBasicBlockValue: Boolean = false
        private set

    private var hasResult: Boolean = false

    private val previousBasicBlockResultVariable: VMVariable.Local by lazy {
        if (!isEntryPoint) {
            val variable = variableCache[Variable.Local(null)]

            val instruction = InstructionWithRegister(null, needsPreviousBasicBlockValue, variable)
            instructions.add(1, instruction)

            needsPreviousBasicBlockValue = true
            variable
        } else {
            throw IllegalStateException("There is no previous basic block.")
        }
    }

    fun prepare() {
        if (!isEntryPoint) {
            instructions.add(InstructionWithRegister(labelCache[basicBlock], true, null))
        }

        basicBlock.instructions.forEach {
            when (it) {
                is Instruction.ConditionalJump -> {
                    putOnStack(it.condition)
                    addInstruction(VMInstruction.Branch(labelCache[it.thenBranch]), it)
                    addInstruction(VMInstruction.Jump(labelCache[it.elseBranch]), it)
                }
                is Instruction.Jump -> {
                    if (it.result != null) {
                        putOnStack(it.result)
                        hasResult = true
                    }
                    addInstruction(VMInstruction.Jump(labelCache[it.basicBlock]), it)
                }
                is Instruction.Return -> {
                    putOnStack(it.result)
                    addInstruction(VMInstruction.Return, it)
                }
                is Instruction.Store -> {
                    putOnStack(it.value)

                    when (val variable = variableCache[it.variable]) {
                        is VMVariable.Global -> addInstruction(VMInstruction.SetGlobal(variable), it)
                        is VMVariable.Local -> addInstruction(VMInstruction.SetLocal(variable), it)
                    }
                }
                is Instruction.Print -> {
                    putOnStack(*it.arguments.toTypedArray())
                    addInstruction(VMInstruction.Print(it.format, it.arguments.size), it)
                }
                is Instruction.FunctionCall -> {
                    putOnStack(*it.arguments.toTypedArray())
                    addInstruction(VMInstruction.FunctionCall(functionCache[it.name]), it)
                }
                is Instruction.MethodCall -> {
                    putOnStack(it.receiver, *it.arguments.toTypedArray())
                    addInstruction(VMInstruction.MethodCall(it.name, it.arguments.size + 1), it)
                }
                is Instruction.GetField -> {
                    putOnStack(it.receiver)
                    addInstruction(VMInstruction.GetField(it.name), it)
                }
                is Instruction.SetField -> {
                    putOnStack(it.receiver, it.value)
                    addInstruction(VMInstruction.SetField(it.name), it)
                }
                is Instruction.Array -> {
                    putOnStack(it.size, it.initialValue)
                    addInstruction(VMInstruction.Array, it)
                }
                is Instruction.Object -> {
                    putOnStack(it.parent, *it.fields.map { field -> field.value }.toTypedArray())
                    val fields = it.fields.map { field -> field.name }
                    val methods = it.methods.map { function ->
                        FunctionCompiler(function, variableCache.globalVariableCache, functionCache, labelCache).compile()
                    }
                    addInstruction(VMInstruction.Object(fields, methods), it)
                }
            }
        }
    }

    fun compile(keepResult: Boolean): List<VMInstruction> {
        val instructions = mutableListOf<VMInstruction>()

        this.instructions.forEach {
            it.instruction?.let { instruction -> instructions.add(instruction) }
            it.putToVariable?.let { variable ->
                instructions.add(VMInstruction.SetLocal(variable))
            }
            if (!it.keepOnStack && !keepStack) {
                instructions.add(VMInstruction.Drop)
            }
        }

        when {
            hasResult && keepResult -> {
            }
            hasResult && !keepResult -> {
                if (instructions.size > 1) {
                    when (instructions[instructions.lastIndex - 1]) {
                        is VMInstruction.Literal, is VMInstruction.GetLocal, is VMInstruction.GetGlobal -> {
                            instructions.removeAt(instructions.lastIndex - 1)
                        }
                        else -> instructions.add(instructions.lastIndex, VMInstruction.Drop)
                    }
                } else {
                    throw IllegalStateException("Cannot have result without at least two instructions.")
                }
            }
            !hasResult && keepResult -> throw IllegalStateException("Last jump instruction does not have a return value.")
            !hasResult && !keepResult -> {
            }
        }

        return instructions
    }

    private fun putOnStack(vararg valuesArray: Value) {
        val values = valuesArray.toList()
        val minSize = min(values.size, stack.size)
        var bestMatchSize = 0

        for (size in 1..minSize) {
            if (stack.takeLast(size).zip(values.take(size)).all { matchStackValues(it.first.first, it.second) }) {
                bestMatchSize = size
            }
        }

        repeat(bestMatchSize) {
            stack.removeLast().second.keepOnStack = true
        }

        for (i in bestMatchSize until values.size) {
            val instruction = when (val value = values[i]) {
                is Value.Constant -> {
                    when (value) {
                        is Value.Constant.Boolean -> VMInstruction.Literal.Boolean(value.value)
                        is Value.Constant.Integer -> VMInstruction.Literal.Integer(value.value)
                        Value.Constant.Null -> VMInstruction.Literal.Null
                    }
                }
                is Instruction -> VMInstruction.GetLocal(registerCache.variable(value))
                is Value.PreviousBasicBlockResult -> {
                    if (i == 0 && stack.isEmpty() && !needsPreviousBasicBlockValue) {
                        needsPreviousBasicBlockValue = true
                        null
                    } else {
                        VMInstruction.GetLocal(previousBasicBlockResultVariable)
                    }
                }
                is Variable -> {
                    when (val variable = variableCache[value]) {
                        is VMVariable.Global -> VMInstruction.GetGlobal(variable)
                        is VMVariable.Local -> VMInstruction.GetLocal(variable)
                    }
                }
            }

            instruction?.let {
                instructions.add(InstructionWithRegister(instruction, true, null))
            }
        }
    }

    private fun matchStackValues(actual: Value, required: Value): Boolean =
        simplifyValue(actual) == simplifyValue(required)

    private fun simplifyValue(value: Value): Value {
        if (value is Variable) {
            return variableMapping[value]?.let { simplifyValue(it) } ?: value
        }

        if (value is Instruction) {
            return when (value) {
                is Instruction.Print -> Value.Constant.Null
                is Instruction.SetField -> simplifyValue(value.value)
                is Instruction.Store -> simplifyValue(value.value)
                is Instruction.ConditionalJump, is Instruction.Jump -> throw IllegalStateException("Jump cannot be a value on stack.")
                else -> value
            }
        }

        return value
    }

    private fun addInstruction(vmInstruction: VMInstruction, originalInstruction: Instruction) {
        val instructionWithRegister = InstructionWithRegister(vmInstruction, false, null)

        instructions.add(instructionWithRegister)
        registerCache[originalInstruction] = instructionWithRegister

        if (
            vmInstruction !is VMInstruction.Branch &&
            vmInstruction !is VMInstruction.Jump &&
            vmInstruction !is VMInstruction.Return
        ) {
            stack.add(originalInstruction to instructionWithRegister)
        } else {
            instructionWithRegister.keepOnStack = true
        }

        if (vmInstruction is VMInstruction.FunctionCall || vmInstruction is VMInstruction.MethodCall) {
            variableMapping.toList().forEach { (variable, _) ->
                if (variable is Variable.Global) {
                    variableMapping.remove(variable)
                }
            }
        }
        if (originalInstruction is Instruction.Store) {
            variableMapping[originalInstruction.variable] = originalInstruction.value
        }
    }
}