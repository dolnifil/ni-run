package com.filipdolnik.cvut.run.compiler.ir

class BasicBlock {

    val instructions: List<Instruction>
        get() = mutableInstructions

    private val mutableInstructions: MutableList<Instruction> = mutableListOf()

    val isTerminated: Boolean
        get() = instructions.lastOrNull()?.isTerminating ?: false

    fun addInstruction(instruction: Instruction) {
        if (!isTerminated) {
            mutableInstructions.add(instruction)
        }
    }
}

sealed interface BasicBlockTerminator: Instruction {

    override val isTerminating: Boolean
        get() = true
}

