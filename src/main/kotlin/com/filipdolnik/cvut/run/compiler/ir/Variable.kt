package com.filipdolnik.cvut.run.compiler.ir

import com.filipdolnik.cvut.run.ast.Identifier

sealed interface Variable: Value {

    class Global(val identifier: Identifier?): Variable

    class Local(val identifier: Identifier?): Variable
}
