package com.filipdolnik.cvut.run.compiler.target

class VMProgram(val functions: List<VMFunction>, val entryPoint: VMFunction)