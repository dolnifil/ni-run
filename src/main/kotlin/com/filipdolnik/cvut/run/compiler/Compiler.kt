package com.filipdolnik.cvut.run.compiler

import com.filipdolnik.cvut.run.ast.AstNode
import com.filipdolnik.cvut.run.compiler.target.ProgramSerializer
import com.filipdolnik.cvut.run.compiler.target.backend.VMBackend

class Compiler(private val program: AstNode) {

    fun compile(): UByteArray {
        val ir = IRConvertor(program).convert()

        val program = VMBackend().compile(ir)

        return ProgramSerializer().serialize(program)
    }
}