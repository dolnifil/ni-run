package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.compiler.ir.Program
import com.filipdolnik.cvut.run.compiler.target.VMProgram

class VMBackend {

    fun compile(program: Program): VMProgram {
        val globalVariableCache = GlobalVariableCache(program)
        val functionCache = FunctionCache(program)
        val labelCache = LabelCache()

        val entryPoint = FunctionCompiler(program.entryPoint, globalVariableCache, functionCache, labelCache, true).compile()

        val functions = program.functions.map {
            FunctionCompiler(it, globalVariableCache, functionCache, labelCache).compile()
        }

        return VMProgram(functions, entryPoint)
    }
}