package com.filipdolnik.cvut.run.compiler

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.BasicBlock
import com.filipdolnik.cvut.run.compiler.ir.Function
import com.filipdolnik.cvut.run.compiler.ir.Instruction
import com.filipdolnik.cvut.run.compiler.ir.Program
import com.filipdolnik.cvut.run.compiler.ir.Value
import com.filipdolnik.cvut.run.compiler.ir.Variable

class IRConvertorContext {

    private val entryPoint = BasicBlock()
    private var currentBasicBlock = entryPoint

    private val functions = mutableListOf<Function>()

    private val globalScope = Scope.Global()
    private var currentScope: Scope = globalScope

    val isGlobalScope: Boolean
        get() = currentScope == globalScope

    fun buildProgram(): Program =
        Program(
            globalScope.globalVariables,
            functions,
            Function(null, emptyList(), globalScope.localVariables, entryPoint)
        )

    fun addInstruction(instruction: Instruction): Instruction {
        currentBasicBlock.addInstruction(instruction)

        return instruction
    }

    fun declareVariable(identifier: Identifier?): Variable = currentScope.declareVariable(identifier)

    fun variable(identifier: Identifier): Variable = currentScope[identifier]

    fun scoped(scope: () -> Value): Value = withTemporaryScope(currentScope.nested(), scope)

    fun declareFunction(
        name: Identifier,
        parameters: List<Identifier>,
        function: () -> Value,
    ): Function = createFunction(name, parameters, function).also {
        functions.add(it)
    }

    fun createFunction(
        name: Identifier,
        parameters: List<Identifier>,
        function: () -> Value,
    ): Function {
        val previousBasicBlock = currentBasicBlock
        val entryPointBlock = BasicBlock()
        currentBasicBlock = entryPointBlock

        val scope = Scope.Function(currentScope, parameters)

        withTemporaryScope(scope, function)

        currentBasicBlock = previousBasicBlock

        return Function(name, scope.parameters, scope.localVariables, entryPointBlock)
    }

    fun openBasicBlock(basicBlock: BasicBlock) {
        currentBasicBlock = basicBlock
    }

    private fun withTemporaryScope(scope: Scope, block: () -> Value): Value {
        val previousScope = currentScope
        currentScope = scope

        val result = block()

        currentScope = previousScope

        return result
    }
}