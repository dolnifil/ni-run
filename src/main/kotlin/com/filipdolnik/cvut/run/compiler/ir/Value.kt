package com.filipdolnik.cvut.run.compiler.ir

sealed interface Value {

    sealed interface Constant: Value {

        inline class Integer(val value: Int): Constant

        inline class Boolean(val value: kotlin.Boolean): Constant

        object Null: Constant
    }

    object PreviousBasicBlockResult: Value
}
