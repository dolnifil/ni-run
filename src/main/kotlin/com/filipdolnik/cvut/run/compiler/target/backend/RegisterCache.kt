package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.compiler.ir.Instruction
import com.filipdolnik.cvut.run.compiler.ir.Variable
import com.filipdolnik.cvut.run.compiler.target.VMVariable

class RegisterCache(private val variableCache: VariableCache) {

    private val instructionMap = mutableMapOf<Instruction, InstructionWithRegister>()
    private val instructionRegister = mutableMapOf<Instruction, VMVariable.Local>()

    operator fun set(instruction: Instruction, instructionWithRegister: InstructionWithRegister) {
        instructionWithRegister.putToVariable = instructionRegister[instruction]

        instructionMap.putIfAbsent(instruction, instructionWithRegister)
    }

    fun variable(instruction: Instruction): VMVariable.Local =
        instructionRegister.getOrPut(instruction) {
            val variable = variableCache[Variable.Local(null)]
            instructionMap[instruction]?.putToVariable = variable
            variable
        }
}