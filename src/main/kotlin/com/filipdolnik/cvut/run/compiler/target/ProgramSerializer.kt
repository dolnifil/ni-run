package com.filipdolnik.cvut.run.compiler.target

import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.vm.code.InstructionPointer
import com.filipdolnik.cvut.run.vm.constant.ConstantPool
import com.filipdolnik.cvut.run.vm.constant.ProgramObject

class ProgramSerializer {

    fun serialize(program: VMProgram): UByteArray {
        val builder = ConstantPoolBuilder(program)
        builder.createConstantPool()

        val serializer = StatefulSerializer(program, builder)

        return serializer.serialize()
    }

    private class ConstantPoolBuilder(private val program: VMProgram) {

        private val builtInMethods = mutableSetOf(
            "==", "eq",
            "!=", "neq",
            "+", "add",
            "-", "sub",
            "*", "mul",
            "/", "div",
            "%", "mod",
            "<=", "le",
            ">=", "ge",
            "<", "lt",
            ">", "gt",
            "&", "and",
            "|", "or",
            "get", "set",
        )

        val constantPool = mutableListOf<ProgramObject>()

        val functionDefinitionByPointer = mutableMapOf<InstructionPointer, VMFunction>()

        val globalVariables = mutableMapOf<VMVariable.Global, UShort>()
        val functionMapping = mutableMapOf<VMFunction, UShort>()
        var entryPointIndex: UShort? = null

        val identifierMapping = mutableMapOf<Identifier, UShort>()
        val literalMapping = mutableMapOf<VMInstruction.Literal, UShort>()
        val formatMapping = mutableMapOf<String, UShort>()
        val objectMapping = mutableMapOf<VMInstruction.Object, UShort>()

        fun createConstantPool() {
            program.functions.forEach {
                functionMapping[it] = it.register().value
            }

            entryPointIndex = program.entryPoint.register().value
        }
        
        private fun VMFunction.register(): ConstantPool.Index {
            val nameIndex = identifier.register()

            code.forEach { instruction ->
                instruction.register()
            }

            val method = ProgramObject.Method(
                nameIndex,
                ProgramObject.Method.ArgumentCount(parameters.size.toUByte()),
                ProgramObject.Method.LocalVariableCount(localVariables.size.toUShort()),
                InstructionPointer(functionDefinitionByPointer.size),
            )

            val index = ConstantPool.Index(constantPool.size.toUShort())
            constantPool.add(method)
            functionDefinitionByPointer[method.entryPoint] = this

            return index
        }

        private fun Identifier.register(): ConstantPool.Index = (identifierMapping[this] ?: kotlin.run {
            val index = constantPool.size.toUShort()
            identifierMapping[this] = index
            constantPool.add(ProgramObject.String(name))
            index
        }).let { ConstantPool.Index(it) }

        private fun VMInstruction.register() {
            when (this) {
                is VMInstruction.Literal -> {
                    if (this !in literalMapping) {
                        literalMapping[this] = constantPool.size.toUShort()
                        val literal = when (this) {
                            is VMInstruction.Literal.Boolean -> ProgramObject.Boolean(value)
                            is VMInstruction.Literal.Integer -> ProgramObject.Integer(value)
                            VMInstruction.Literal.Null -> ProgramObject.Null
                        }
                        constantPool.add(literal)
                    }
                }
                is VMInstruction.GetGlobal -> {
                    variable.register()
                }
                is VMInstruction.SetGlobal -> {
                    variable.register()
                }
                is VMInstruction.Label -> {
                    name.register()
                }
                is VMInstruction.MethodCall -> {
                    if (name.name in builtInMethods) {
                        name.register()
                    }
                }
                is VMInstruction.Print -> {
                    if (format !in formatMapping) {
                        formatMapping[format] = constantPool.size.toUShort()
                        constantPool.add(ProgramObject.String(format))
                    }
                }
                is VMInstruction.Object -> {
                    val fields = this.fields.map {
                        val slot = ProgramObject.Slot(it.register())
                        val index = ConstantPool.Index(constantPool.size.toUShort())
                        constantPool.add(slot)
                        index
                    }
                    val methods = this.methods.map {
                        it.register()
                    }

                    objectMapping[this] = constantPool.size.toUShort()
                    constantPool.add(ProgramObject.Class(fields + methods))
                }
                else -> {
                }
            }
        }

        private fun VMVariable.Global.register() {
            if (this !in globalVariables) {
                constantPool.add(ProgramObject.Slot(identifier.register()))
                globalVariables[this] = (constantPool.size - 1).toUShort()
            }
        }
    }

    private class StatefulSerializer(private val program: VMProgram, private val constantPoolBuilder: ConstantPoolBuilder) {

        private val output = mutableListOf<UByte>()

        fun serialize(): UByteArray {
            serializeConstantPool()
            serializeGlobals()
            serializeEntryPoint()

            return output.toUByteArray()
        }

        private fun serializeConstantPool() {
            constantPoolBuilder.constantPool.size.toUShort().serialize()

            constantPoolBuilder.constantPool.forEach {
                it.serialize()
            }
        }

        private fun ProgramObject.serialize() {
            when (this) {
                is ProgramObject.Integer -> {
                    serializeTag(0x00)
                    value.serialize()
                }
                is ProgramObject.Boolean -> {
                    serializeTag(0x06)
                    if (value) {
                        serializeTag(0x01)
                    } else {
                        serializeTag(0x00)
                    }
                }
                ProgramObject.Null -> serializeTag(0x01)
                is ProgramObject.String -> {
                    serializeTag(0x02)
                    value.length.serialize()
                    output.addAll(value.toByteArray().toUByteArray())
                }
                is ProgramObject.Slot -> {
                    serializeTag(0x04)
                    nameIndex.value.serialize()
                }
                is ProgramObject.Method -> serialize()
                is ProgramObject.Class -> {
                    serializeTag(0x05)
                    members.size.toUShort().serialize()
                    members.forEach {
                        it.value.serialize()
                    }
                }
            }
        }

        private fun ProgramObject.Method.serialize() {
            serializeTag(0x03)
            nameIndex.value.serialize()
            argumentCount.value.serialize()
            localVariableCount.value.serialize()

            val function = constantPoolBuilder.functionDefinitionByPointer[entryPoint]
                ?: throw IllegalStateException("Function $this was not serialized.")

            function.code.size.serialize()
            function.code.forEach {
                it.serialize(function)
            }
        }

        private fun VMInstruction.serialize(function: VMFunction) {
            when (this) {
                is VMInstruction.Literal -> {
                    serializeTag(0x01)
                    constantPoolBuilder.literalMapping[this]?.serialize()
                        ?: throw IllegalStateException("Literal $this was not serialized.")
                }
                is VMInstruction.GetLocal -> {
                    serializeTag(0x0A)
                    variable.index.serialize()
                }
                is VMInstruction.SetLocal -> {
                    serializeTag(0x09)
                    variable.index.serialize()
                }
                is VMInstruction.GetGlobal -> {
                    serializeTag(0x0C)
                    constantPoolBuilder.identifierMapping[variable.identifier]?.serialize()
                        ?: throw IllegalStateException("Variable $this was not serialized.")
                }
                is VMInstruction.SetGlobal -> {
                    serializeTag(0x0B)
                    constantPoolBuilder.identifierMapping[variable.identifier]?.serialize()
                        ?: throw IllegalStateException("Variable $this was not serialized.")
                }
                is VMInstruction.FunctionCall -> {
                    serializeTag(0x08)
                    constantPoolBuilder.identifierMapping[this.function.identifier]?.serialize()
                        ?: throw IllegalStateException("Identifier ${function.identifier} was not serialized.")
                    this.function.parameters.size.toUByte().serialize()
                }
                VMInstruction.Return -> {
                    serializeTag(0x0F)
                }
                is VMInstruction.Label -> {
                    serializeTag(0x00)
                    constantPoolBuilder.identifierMapping[name]?.serialize()
                        ?: throw IllegalStateException("Label $this was not serialized.")
                }
                is VMInstruction.Jump -> {
                    serializeTag(0x0E)
                    constantPoolBuilder.identifierMapping[label.name]?.serialize()
                        ?: throw IllegalStateException("Label $this was not serialized.")
                }
                is VMInstruction.Branch -> {
                    serializeTag(0x0D)
                    constantPoolBuilder.identifierMapping[thenLabel.name]?.serialize()
                        ?: throw IllegalStateException("Label $this was not serialized.")
                }
                is VMInstruction.Print -> {
                    serializeTag(0x02)
                    constantPoolBuilder.formatMapping[format]?.serialize()
                        ?: throw IllegalStateException("Format $this was not serialized.")
                    parameterCount.toUByte().serialize()
                }
                VMInstruction.Array -> {
                    serializeTag(0x03)
                }
                is VMInstruction.Object -> {
                    serializeTag(0x04)
                    constantPoolBuilder.objectMapping[this]?.serialize() ?: throw IllegalStateException("Object $this was not serialized.")
                }
                is VMInstruction.GetField -> {
                    serializeTag(0x05)
                    constantPoolBuilder.identifierMapping[name]?.serialize()
                        ?: throw IllegalStateException("Field $this was not serialized.")
                }
                is VMInstruction.SetField -> {
                    serializeTag(0x06)
                    constantPoolBuilder.identifierMapping[name]?.serialize()
                        ?: throw IllegalStateException("Field $this was not serialized.")
                }
                is VMInstruction.MethodCall -> {
                    serializeTag(0x07)
                    constantPoolBuilder.identifierMapping[name]?.serialize()
                        ?: throw IllegalStateException("Identifier $name was not serialized.")
                    parameterCount.toUByte().serialize()
                }
                VMInstruction.Drop -> {
                    serializeTag(0x10)
                }
            }
        }

        private fun serializeGlobals() {
            (constantPoolBuilder.globalVariables.size + constantPoolBuilder.functionMapping.size).toUShort().serialize()

            constantPoolBuilder.globalVariables.forEach {
                it.value.serialize()
            }

            constantPoolBuilder.functionMapping.forEach {
                it.value.serialize()
            }
        }

        private fun serializeEntryPoint() {
            constantPoolBuilder.entryPointIndex?.serialize()
                ?: throw IllegalStateException("Program $program entry point was not serialized.")
        }

        private fun serializeTag(tag: Byte) {
            tag.toUByte().serialize()
        }

        private fun UByte.serialize() {
            output.add(this)
        }

        private fun UShort.serialize() {
            output.add((this % 256u).toUByte())
            output.add((this / 256u).toUByte())
        }

        private fun Int.serialize() {
            output.add(and(255).toUByte())
            output.add(shr(8).and(255).toUByte())
            output.add(shr(16).and(255).toUByte())
            output.add(shr(24).and(255).toUByte())
        }
    }
}