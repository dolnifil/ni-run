package com.filipdolnik.cvut.run.compiler

import com.filipdolnik.cvut.run.ast.AstNode
import com.filipdolnik.cvut.run.ast.Identifier
import com.filipdolnik.cvut.run.compiler.ir.BasicBlock
import com.filipdolnik.cvut.run.compiler.ir.Function
import com.filipdolnik.cvut.run.compiler.ir.Instruction
import com.filipdolnik.cvut.run.compiler.ir.Program
import com.filipdolnik.cvut.run.compiler.ir.Value
import com.filipdolnik.cvut.run.compiler.ir.Variable

class IRConvertor(private val program: AstNode) {

    private val context = IRConvertorContext()

    fun convert(): Program {
        program.convert()

        return context.buildProgram()
    }

    private fun AstNode.convert(): Value = when (this) {
        is AstNode.Integer -> convert()
        is AstNode.Boolean -> convert()
        AstNode.Null -> Value.Constant.Null
        is AstNode.VariableDefinition -> convert()
        is AstNode.VariableAccess -> convert()
        is AstNode.VariableAssignment -> convert()
        is AstNode.Array -> convert()
        is AstNode.ArrayAccess -> convert()
        is AstNode.ArrayAssignment -> convert()
        is AstNode.Function -> convert()
        is AstNode.FunctionCall -> convert()
        is AstNode.Print -> convert()
        is AstNode.Block -> convert()
        is AstNode.Top -> convert()
        is AstNode.While -> convert()
        is AstNode.If -> convert()
        is AstNode.Object -> convert()
        is AstNode.FieldAssignment -> convert()
        is AstNode.FieldAccess -> convert()
        is AstNode.MethodCall -> convert()
    }

    private fun AstNode.Integer.convert(): Value = Value.Constant.Integer(value)

    private fun AstNode.Boolean.convert(): Value = Value.Constant.Boolean(value)

    private fun AstNode.VariableDefinition.convert(): Value {
        val initialValue = value.convert()

        val variable = if (context.isGlobalScope) variable(name) else declareVariable(name)

        return addInstruction(Instruction.Store(variable, initialValue))
    }

    private fun AstNode.VariableAccess.convert(): Value =
        variable(name)

    private fun AstNode.VariableAssignment.convert(): Value {
        val newValue = value.convert()

        val variable = variable(name)

        return addInstruction(Instruction.Store(variable, newValue))
    }

    private fun AstNode.Array.convert(): Value {
        val size = size.convert()

        val isConstant = value is AstNode.Integer || value is AstNode.Boolean || value is AstNode.Null
        val initialValue = if (isConstant) value.convert() else Value.Constant.Null

        val array = addInstruction(Instruction.Array(size, initialValue))

        if (!isConstant) {
            val index = declareVariable(null)

            addInstruction(Instruction.Store(index, Value.Constant.Integer(0)))

            val conditionBlock = BasicBlock()
            val loopBlock = BasicBlock()
            val afterBlock = BasicBlock()

            addInstruction(Instruction.Jump(conditionBlock, null))
            openBasicBlock(conditionBlock)

            val condition = addInstruction(Instruction.MethodCall(Identifier("<"), index, listOf(size)))

            addInstruction(Instruction.ConditionalJump(condition, loopBlock, afterBlock))
            openBasicBlock(loopBlock)

            val value = scoped {
                value.convert()
            }
            addInstruction(Instruction.MethodCall(Identifier("set"), array, listOf(index, value)))

            val incrementedIndex = addInstruction(Instruction.MethodCall(Identifier("+"), index, listOf(Value.Constant.Integer(1))))
            addInstruction(Instruction.Store(index, incrementedIndex))

            addInstruction(Instruction.Jump(conditionBlock, null))
            openBasicBlock(afterBlock)
        }

        return array
    }

    private fun AstNode.ArrayAccess.convert(): Value {
        val receiver = array.convert()

        val index = index.convert()

        return addInstruction(Instruction.MethodCall(Identifier("get"), receiver, listOf(index)))
    }

    private fun AstNode.ArrayAssignment.convert(): Value {
        val receiver = array.convert()

        val index = index.convert()
        val value = value.convert()

        return addInstruction(Instruction.MethodCall(Identifier("set"), receiver, listOf(index, value)))
    }

    private fun AstNode.Function.convert(): Value {
        declareFunction(name, parameters) {
            val result = body.convert()
            addInstruction(Instruction.Return(result))
        }

        return Value.Constant.Null
    }

    private fun AstNode.FunctionCall.convert(): Value {
        val arguments = arguments.map { it.convert() }

        return addInstruction(Instruction.FunctionCall(name, arguments))
    }

    private fun AstNode.Print.convert(): Value {
        val arguments = arguments.map { it.convert() }

        return addInstruction(Instruction.Print(format, arguments))
    }

    private fun AstNode.Block.convert(): Value = scoped {
        var result: Value = Value.Constant.Null

        body.forEach {
            result = it.convert()
        }

        result
    }

    private fun AstNode.Top.convert(): Value {
        body.filterIsInstance<AstNode.VariableDefinition>().forEach {
            declareVariable(it.name)
        }

        body.forEach {
            it.convert()
        }

        return Value.Constant.Null
    }

    private fun AstNode.While.convert(): Value {
        val loopBlock = BasicBlock()
        val conditionBlock = BasicBlock()
        val afterBlock = BasicBlock()

        addInstruction(Instruction.Jump(conditionBlock, null))
        openBasicBlock(conditionBlock)

        val condition = condition.convert()
        addInstruction(Instruction.ConditionalJump(condition, loopBlock, afterBlock))

        openBasicBlock(loopBlock)
        body.convert()
        addInstruction(Instruction.Jump(conditionBlock, null))

        openBasicBlock(afterBlock)

        return Value.Constant.Null
    }

    private fun AstNode.If.convert(): Value {
        val condition = condition.convert()

        val thenBlock = BasicBlock()
        val elseBlock = BasicBlock()
        val afterBlock = BasicBlock()

        addInstruction(Instruction.ConditionalJump(condition, thenBlock, elseBlock))

        openBasicBlock(thenBlock)
        val thenBranchResult = thenBranch.convert()
        addInstruction(Instruction.Jump(afterBlock, thenBranchResult))

        openBasicBlock(elseBlock)
        val elseBranchResult = elseBranch.convert()
        addInstruction(Instruction.Jump(afterBlock, elseBranchResult))

        openBasicBlock(afterBlock)

        return Value.PreviousBasicBlockResult
    }

    private fun AstNode.Object.convert(): Value {
        val parent = parent.convert()

        val fields = body.filterIsInstance<AstNode.VariableDefinition>().map {
            val value = it.value.convert()

            Instruction.Object.Field(it.name, value)
        }

        val methods = body.filterIsInstance<AstNode.Function>().map {
            createFunction(it.name, listOf(Identifier("this")) + it.parameters) {
                val result = it.body.convert()
                addInstruction(Instruction.Return(result))
            }
        }

        return addInstruction(Instruction.Object(parent, fields, methods))
    }

    private fun AstNode.FieldAssignment.convert(): Value {
        val receiver = receiver.convert()

        val value = value.convert()

        return addInstruction(Instruction.SetField(field, receiver, value))
    }

    private fun AstNode.FieldAccess.convert(): Value {
        val receiver = receiver.convert()

        return addInstruction(Instruction.GetField(field, receiver))
    }

    private fun AstNode.MethodCall.convert(): Value {
        val receiver = receiver.convert()

        val arguments = arguments.map { it.convert() }

        return addInstruction(Instruction.MethodCall(name, receiver, arguments))
    }

    private fun addInstruction(instruction: Instruction): Instruction =
        context.addInstruction(instruction)

    private fun declareVariable(identifier: Identifier?): Variable =
        context.declareVariable(identifier)

    private fun variable(identifier: Identifier): Variable =
        context.variable(identifier)

    private fun scoped(scope: () -> Value): Value =
        context.scoped(scope)

    private fun declareFunction(name: Identifier, parameters: List<Identifier>, function: () -> Value): Function =
        context.declareFunction(name, parameters, function)

    private fun createFunction(name: Identifier, parameters: List<Identifier>, function: () -> Value): Function =
        context.createFunction(name, parameters, function)

    private fun openBasicBlock(basicBlock: BasicBlock) =
        context.openBasicBlock(basicBlock)
}