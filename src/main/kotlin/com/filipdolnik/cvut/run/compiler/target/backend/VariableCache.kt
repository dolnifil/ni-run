package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.compiler.ir.Function
import com.filipdolnik.cvut.run.compiler.ir.Variable
import com.filipdolnik.cvut.run.compiler.target.VMVariable

class VariableCache(val globalVariableCache: GlobalVariableCache, private val function: Function) {

    val parameters: List<VMVariable.Local>
        get() = function.parameters.map { get(it) }

    val localVariables: List<VMVariable.Local>
        get() = (map.values - parameters).sortedBy { it.index }

    private val map = mutableMapOf<Variable.Local, VMVariable.Local>()

    init {
        (function.parameters + function.localVariables).forEach {
            map[it] = VMVariable.Local(map.size.toUShort())
        }
    }

    operator fun get(variable: Variable.Local): VMVariable.Local = map.getOrPut(variable) {
        VMVariable.Local(map.size.toUShort())
    }

    operator fun get(variable: Variable): VMVariable = when (variable) {
        is Variable.Global -> globalVariableCache[variable]
        is Variable.Local -> get(variable)
    }
}