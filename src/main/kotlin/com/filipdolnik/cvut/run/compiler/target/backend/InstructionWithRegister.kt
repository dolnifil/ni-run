package com.filipdolnik.cvut.run.compiler.target.backend

import com.filipdolnik.cvut.run.compiler.target.VMInstruction
import com.filipdolnik.cvut.run.compiler.target.VMVariable

class InstructionWithRegister(val instruction: VMInstruction?, var keepOnStack: Boolean, var putToVariable: VMVariable.Local?)