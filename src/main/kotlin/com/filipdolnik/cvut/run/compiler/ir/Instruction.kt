package com.filipdolnik.cvut.run.compiler.ir

import com.filipdolnik.cvut.run.ast.Identifier

sealed interface Instruction: Value {

    val isTerminating: Boolean
        get() = false

    class ConditionalJump(val condition: Value, val thenBranch: BasicBlock, val elseBranch: BasicBlock): BasicBlockTerminator

    class Jump(val basicBlock: BasicBlock, val result: Value?): BasicBlockTerminator

    class Return(val result: Value): BasicBlockTerminator

    class Store(val variable: Variable, val value: Value): Instruction

    class Print(val format: String, val arguments: List<Value>): Instruction

    class FunctionCall(val name: Identifier, val arguments: List<Value>): Instruction

    class MethodCall(val name: Identifier, val receiver: Value, val arguments: List<Value>): Instruction

    class GetField(val name: Identifier, val receiver: Value): Instruction

    class SetField(val name: Identifier, val receiver: Value, val value: Value): Instruction

    class Array(val size: Value, val initialValue: Value): Instruction

    class Object(val parent: Value, val fields: List<Field>, val methods: List<Function>): Instruction {

        class Field(val name: Identifier, val value: Value)
    }
}