package com.filipdolnik.cvut.run

import com.filipdolnik.cvut.run.compiler.Compiler
import com.filipdolnik.cvut.run.parser.JsonParser
import org.junit.jupiter.api.Test
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.forEachDirectoryEntry
import kotlin.io.path.isRegularFile
import kotlin.io.path.writeBytes

class CompilerTest {

    @ExperimentalPathApi
    @Test
    fun compileAll() {
        Path("tests").forEachDirectoryEntry {
            if (it.isRegularFile()) {
                println(it.toFile().name)

                val path = Path("FML/target/release/fml.exe").toFile().canonicalPath
                val process = ProcessBuilder(path, "parse", it.toFile().canonicalPath, "--format=JSON")
                    .start()
                val json = process.inputStream.readAllBytes().decodeToString()
                process.waitFor()
                val error = process.errorStream.readAllBytes().decodeToString()

                if (error.isNotBlank()) {
                    System.err.println(error)
                } else {
                    val code = JsonParser().parse(json)

                    val compiler = Compiler(code)

                    val bytecode = compiler.compile()

                    Path(it.parent.toFile().canonicalPath, "test", it.toFile().name.dropLast(4) + ".bc").writeBytes(bytecode.toByteArray())
                }
            }
        }
    }
}