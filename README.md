Build: `./build.sh`
Run: `./fml.sh file`

It might be necessary to install JDK 8, but it should be present on Ubuntu by default. 
If not run something like `sudo apt-get install openjdk-8-jdk` (or use SDKMAN which may be handy if you need multiple versions of JDK). 

Performance report is in the directory "performance".